USE gift_certificates_db;

--Mysql Basic Schema
-- Table for gift_certificate
CREATE TABLE gift_certificate (
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    price DECIMAL(19,2) NOT NULL,
    duration INT NOT NULL,
    create_date TIMESTAMP,
    last_update_date TIMESTAMP,
    PRIMARY KEY (id)
);

-- Table for tag
CREATE TABLE tag (
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

-- Table for gift_certificate_tag
CREATE TABLE gift_certificate_tag (
    gift_certificate_id BIGINT NOT NULL,
    tag_id BIGINT NOT NULL,
    PRIMARY KEY (gift_certificate_id, tag_id),
    FOREIGN KEY (gift_certificate_id) REFERENCES gift_certificate(id),
    FOREIGN KEY (tag_id) REFERENCES tag(id)
);



INSERT INTO gift_certificate
VALUES ( 3,'Gift C 3', 'Gift C 3', 30.00, 30, null, null);

INSERT INTO gift_certificate
VALUES (2,'Gift C 2', 'Gift C 2', 20.00, 20, null, null);

INSERT INTO gift_certificate
VALUES (3, 'Gift C 3', 'Gift C 3', 30.00, 30,null,null);

-- Insert into tag table
INSERT INTO tag
VALUES (1,'Tag 1');

INSERT INTO tag
VALUES (2,'Tag 2');

INSERT INTO tag
VALUES (3, 'Tag 3');
