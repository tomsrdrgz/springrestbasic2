package com.epam.esm.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.WebJarsResourceResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.text.SimpleDateFormat;
import java.util.List;
/**
 * <p>WebConfig class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    /** {@inheritDoc} */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry
                .addResourceHandler("/webjars/**")
                        .addResourceLocations("/webjars/")
                                .resourceChain(true).addResolver(new WebJarsResourceResolver());
        registry
                .addResourceHandler("/static/css/**") // URL pattern to map
                    .addResourceLocations("classpath:/css/"); // Location of your CSS files

    }
    /** {@inheritDoc} */
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>>
                                                   converters) {


        Jackson2ObjectMapperBuilder builder = new
                Jackson2ObjectMapperBuilder();
        builder.indentOutput(true).dateFormat(new
                SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        builder.failOnUnknownProperties(true);
        converters.add(new MappingJackson2HttpMessageConverter(builder.
                build()));

    }
    /**
     * <p>jspViewResolver.</p>
     *
     * @return a {@link org.springframework.web.servlet.view.InternalResourceViewResolver} object
     */
    @Bean
    public InternalResourceViewResolver jspViewResolver() {
        InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setPrefix("/WEB-INF/views/");
        return bean;
    }
}
