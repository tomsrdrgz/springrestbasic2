package com.epam.esm.config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * <p>AppConfig class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */

@Configuration
public class AppConfig {

    /**
     * <p>propertySourcesPlaceholderConfigurer.</p>
     *
     * @return a {@link org.springframework.context.support.PropertySourcesPlaceholderConfigurer} object
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {

        return new PropertySourcesPlaceholderConfigurer();
    }
}

