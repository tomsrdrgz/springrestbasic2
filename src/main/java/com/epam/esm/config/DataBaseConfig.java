package com.epam.esm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;

/**
 * <p>DataBaseConfig class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
@Configuration
@EnableJpaRepositories(basePackages = "com.epam.esm.repository")
@EnableTransactionManagement
@EnableConfigurationProperties(DataBaseProperties.class)
public class DataBaseConfig {

    private final DataBaseProperties dataBaseProperties;

    /**
     * <p>Constructor for DataBaseConfig.</p>
     *
     * @param dataBaseProperties a {@link com.epam.esm.config.DataBaseProperties} object
     */
    @Autowired
    public DataBaseConfig(DataBaseProperties dataBaseProperties) {
        this.dataBaseProperties = dataBaseProperties;
    }

    /**
     * <p>dataSource.</p>
     *
     * @return a DataSource object
     */
    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName(dataBaseProperties.getDriverClassName());
        dataSource.setUrl(dataBaseProperties.getUrl());

        return dataSource;
    }


    /**
     * <p>entityManagerFactory.</p>
     *
     * @return a {@link org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean} object
     */
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
        HashMap<String, String> map = new HashMap<>();
        map.put("hibernate.dialect", dataBaseProperties.getDialect());
        //Set jpa property open in view to true
        map.put("hibernate.show_sql", dataBaseProperties.getShowSql());
        HibernateJpaVendorAdapter vendorAdapter = new
                HibernateJpaVendorAdapter();
        vendorAdapter.setShowSql(true);
        LocalContainerEntityManagerFactoryBean factory = new
                LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setDataSource(dataSource());
        factory.setJpaPropertyMap(map);
        factory.setPackagesToScan("com.epam.esm.repository.entity");

        return factory;
    }

    /**
     * <p>transactionManager.</p>
     *
     * @return a {@link org.springframework.transaction.PlatformTransactionManager} object
     */
    @Bean
    public PlatformTransactionManager transactionManager() {
        return new JpaTransactionManager();
    }

}
