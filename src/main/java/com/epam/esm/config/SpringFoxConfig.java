package com.epam.esm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * <p>SpringFoxConfig class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
@Configuration
@EnableSwagger2
public class SpringFoxConfig {
    /**
     * <p>api.</p>
     *
     * @return a {@link springfox.documentation.spring.web.plugins.Docket} object
     */

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

}
