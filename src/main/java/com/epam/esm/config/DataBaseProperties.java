package com.epam.esm.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


/**
 * <p>DataBaseProperties class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
@Configuration
@ConfigurationProperties(prefix = "database")
@PropertySource("classpath:application-${spring.profiles.active}.properties")
public class DataBaseProperties {

    @Value("${database.driver-class-name}")
    private String driverClassName;

    @Value("${database.url}")
    private String url;

    @Value("${spring.hibernate.dialect}")
    private String dialect;

    @Value("${spring.hibernate.ddl-auto}")
    private String ddlAuto;

    @Value("${spring.hibernate.show-sql}")
    private String showSql;

    /**
     * <p>Getter for the field <code>driverClassName</code>.</p>
     *
     * @return a {@link java.lang.String} object
     */
    public String getDriverClassName() {
        return driverClassName;
    }

    /**
     * <p>Setter for the field <code>driverClassName</code>.</p>
     *
     * @param driverClassName a {@link java.lang.String} object
     */
    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    /**
     * <p>Getter for the field <code>url</code>.</p>
     *
     * @return a {@link java.lang.String} object
     */
    public String getUrl() {
        return url;
    }

    /**
     * <p>Setter for the field <code>url</code>.</p>
     *
     * @param url a {@link java.lang.String} object
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * <p>Getter for the field <code>dialect</code>.</p>
     *
     * @return a {@link java.lang.String} object
     */
    public String getDialect() {
        return dialect;
    }

    /**
     * <p>Setter for the field <code>dialect</code>.</p>
     *
     * @param dialect a {@link java.lang.String} object
     */
    public void setDialect(String dialect) {
        this.dialect = dialect;
    }

    /**
     * <p>Getter for the field <code>ddlAuto</code>.</p>
     *
     * @return a {@link java.lang.String} object
     */
    public String getDdlAuto() {
        return ddlAuto;
    }

    /**
     * <p>Setter for the field <code>ddlAuto</code>.</p>
     *
     * @param ddlAuto a {@link java.lang.String} object
     */
    public void setDdlAuto(String ddlAuto) {
        this.ddlAuto = ddlAuto;
    }

    /**
     * <p>Getter for the field <code>showSql</code>.</p>
     *
     * @return a {@link java.lang.String} object
     */
    public String getShowSql() {
        return showSql;
    }

    /**
     * <p>Setter for the field <code>showSql</code>.</p>
     *
     * @param showSql a {@link java.lang.String} object
     */
    public void setShowSql(String showSql) {
        this.showSql = showSql;
    }
}
