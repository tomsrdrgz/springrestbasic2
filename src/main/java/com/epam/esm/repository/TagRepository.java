package com.epam.esm.repository;

import com.epam.esm.repository.entity.TagEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * <p>TagRepository interface.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
public interface TagRepository extends CrudRepository<TagEntity,Long> {

    /**
     * <p>findByName.</p>
     *
     * @param tagName a {@link java.lang.String} object
     * @return a {@link com.epam.esm.repository.entity.TagEntity} object
     */
    @Query(value = "SELECT * FROM tag WHERE name = :tagName", nativeQuery = true)
    TagEntity findByName(@Param("tagName") String tagName);


    /**
     * <p>findNameContains.</p>
     *
     * @param tagName a {@link java.lang.String} object
     * @return a {@link java.util.List} object
     */
    @Query(value = "SELECT * FROM tag WHERE name LIKE %:tagName%", nativeQuery = true)
    List<TagEntity> findNameContains(@Param("tagName") String tagName);

}
