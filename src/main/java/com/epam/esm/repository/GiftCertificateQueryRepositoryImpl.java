package com.epam.esm.repository;

import com.epam.esm.repository.entity.GiftCertificateEntity;
import com.epam.esm.web.dto.SortBy;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * <p>GiftCertificateQueryRepositoryImpl class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
@Repository
public class GiftCertificateQueryRepositoryImpl implements GiftCertificateQueryRepository {


    private final static String FIND_BY_TAG_NAME_QUERY
            = "SELECT gc FROM GiftCertificateEntity gc JOIN gc.tags t WHERE t.name = :tagName ";
    private final static String FIND_BY_TAG_NAME_OR_NAME_OR_DESCRIPTION_CONTAINING_QUERY
            = "SELECT gc FROM GiftCertificateEntity gc JOIN gc.tags t WHERE t.name = :tagName AND( gc.name LIKE CONCAT('%', :searchString, '%') OR gc.description LIKE CONCAT('%', :searchString, '%'))";
    private final static String FIND_BY_NAME_OR_DESCRIPTION_CONTAINING_QUERY
            = "SELECT gc FROM GiftCertificateEntity gc  WHERE gc.name LIKE CONCAT('%', :searchString, '%') OR gc.description LIKE CONCAT('%', :searchString, '%') \n";
    @PersistenceContext
    private EntityManager entityManager;

    /** {@inheritDoc} */
    @Override
    public List<GiftCertificateEntity> findByTagName(String tagName, SortBy sortBy) {
        String jpql = FIND_BY_TAG_NAME_QUERY + getSortingOrder(sortBy);

        TypedQuery<GiftCertificateEntity> query = entityManager.createQuery(jpql, GiftCertificateEntity.class);
        query.setParameter("tagName", tagName);
        return query.getResultList();
    }

    /** {@inheritDoc} */
    @Override
    public List<GiftCertificateEntity> findByNameOrDescriptionContaining(String searchString, SortBy sortBy) {

        String jpql = FIND_BY_NAME_OR_DESCRIPTION_CONTAINING_QUERY + getSortingOrder(sortBy);

        TypedQuery<GiftCertificateEntity> query = entityManager.createQuery(jpql, GiftCertificateEntity.class);
        query.setParameter("searchString", searchString);
        return query.getResultList();
    }




    /** {@inheritDoc} */
    @Override
    public List<GiftCertificateEntity> findByTagAndNameOrDescriptionContaining(String tagName, String searchString, SortBy sortBy) {

        String jpql = FIND_BY_TAG_NAME_OR_NAME_OR_DESCRIPTION_CONTAINING_QUERY + getSortingOrder(sortBy);

        TypedQuery<GiftCertificateEntity> query = entityManager.createQuery(jpql, GiftCertificateEntity.class);
        query.setParameter("searchString", searchString);
        query.setParameter("tagName", tagName);
        return query.getResultList();

    }
    private static String getSortingOrder(SortBy sortBy) {
        if (sortBy == SortBy.NAME_ASC) {
            return  " ORDER BY gc.name ASC";
        } else if (sortBy == SortBy.NAME_DESC) {
            return " ORDER BY gc.name DESC";
        } else if (sortBy == SortBy.DATE_DESC) {
            return " ORDER BY gc.createDate DESC";
        } else {
            return " ORDER BY gc.createDate ASC";
        }
    }

    /** {@inheritDoc} */
    @Override
    public List<GiftCertificateEntity> findAll() {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public List<GiftCertificateEntity> findAll(Sort sort) {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public Page<GiftCertificateEntity> findAll(Pageable pageable) {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public List<GiftCertificateEntity> findAll(Iterable<Long> longs) {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public long count() {
        return 0;
    }

    /** {@inheritDoc} */
    @Override
    public void delete(Long aLong) {

    }

    /** {@inheritDoc} */
    @Override
    public void delete(GiftCertificateEntity entity) {

    }

    /** {@inheritDoc} */
    @Override
    public void delete(Iterable<? extends GiftCertificateEntity> entities) {

    }

    /** {@inheritDoc} */
    @Override
    public void deleteAll() {

    }

    /** {@inheritDoc} */
    @Override
    public <S extends GiftCertificateEntity> S save(S entity) {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public <S extends GiftCertificateEntity> List<S> save(Iterable<S> entities) {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public GiftCertificateEntity findOne(Long aLong) {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public boolean exists(Long aLong) {
        return false;
    }

    /** {@inheritDoc} */
    @Override
    public void flush() {

    }

    /** {@inheritDoc} */
    @Override
    public <S extends GiftCertificateEntity> S saveAndFlush(S entity) {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public void deleteInBatch(Iterable<GiftCertificateEntity> entities) {

    }

    /** {@inheritDoc} */
    @Override
    public void deleteAllInBatch() {

    }

    /** {@inheritDoc} */
    @Override
    public GiftCertificateEntity getOne(Long aLong) {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public <S extends GiftCertificateEntity> S findOne(Example<S> example) {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public <S extends GiftCertificateEntity> List<S> findAll(Example<S> example) {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public <S extends GiftCertificateEntity> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public <S extends GiftCertificateEntity> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    /** {@inheritDoc} */
    @Override
    public <S extends GiftCertificateEntity> long count(Example<S> example) {
        return 0;
    }

    /** {@inheritDoc} */
    @Override
    public <S extends GiftCertificateEntity> boolean exists(Example<S> example) {
        return false;
    }
}

