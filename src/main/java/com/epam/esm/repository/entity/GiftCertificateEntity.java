package com.epam.esm.repository.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;




/**
 * <p>GiftCertificateEntity class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */


@Entity
@Table(name = "gift_certificate")
@JsonRootName(value = "gift_certificate")
public class GiftCertificateEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToMany(cascade = { CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JoinTable(name = "gift_certificate_tag",
            joinColumns = { @JoinColumn(name = "gift_certificate_id")},
            inverseJoinColumns = { @JoinColumn(name ="tag_id")})
    @JsonIgnoreProperties("giftCertificates")
    Set<TagEntity> tags = new HashSet<>();

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "duration")
    private Integer duration;

    @CreationTimestamp
    @Column(name = "create_date")
    private Timestamp createDate;

    @UpdateTimestamp
    @Column(name = "last_update_date")
    private Timestamp lastUpdateDate;


    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Long} object
     */
    public Long getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Long} object
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>name</code>.</p>
     *
     * @return a {@link java.lang.String} object
     */
    public String getName() {
        return name;
    }

    /**
     * <p>Setter for the field <code>name</code>.</p>
     *
     * @param name a {@link java.lang.String} object
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the field <code>description</code>.</p>
     *
     * @return a {@link java.lang.String} object
     */
    public String getDescription() {
        return description;
    }

    /**
     * <p>Setter for the field <code>description</code>.</p>
     *
     * @param description a {@link java.lang.String} object
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * <p>Getter for the field <code>price</code>.</p>
     *
     * @return a {@link java.math.BigDecimal} object
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * <p>Setter for the field <code>price</code>.</p>
     *
     * @param price a {@link java.math.BigDecimal} object
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * <p>Getter for the field <code>duration</code>.</p>
     *
     * @return a {@link java.lang.Integer} object
     */
    public Integer getDuration() {
        return duration;
    }

    /**
     * <p>Setter for the field <code>duration</code>.</p>
     *
     * @param duration a {@link java.lang.Integer} object
     */
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /**
     * <p>Getter for the field <code>createDate</code>.</p>
     *
     * @return a Timestamp object
     */
    public Timestamp getCreateDate() {
        return createDate;
    }

    /**
     * <p>Setter for the field <code>createDate</code>.</p>
     *
     * @param createDate a Timestamp object
     */
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    /**
     * <p>Getter for the field <code>lastUpdateDate</code>.</p>
     *
     * @return a Timestamp object
     */
    public Timestamp getLastUpdateDate() {
        return lastUpdateDate;
    }

    /**
     * <p>Setter for the field <code>lastUpdateDate</code>.</p>
     *
     * @param lastUpdateDate a Timestamp object
     */
    public void setLastUpdateDate(Timestamp lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    /**
     * <p>Getter for the field <code>tags</code>.</p>
     *
     * @return a {@link java.util.Set} object
     */
    public Set<TagEntity> getTags() {
        return tags;
    }

    /**
     * <p>Setter for the field <code>tags</code>.</p>
     *
     * @param tags a {@link java.util.Set} object
     */
    public void setTags(Set<TagEntity> tags) {
        this.tags = tags;
    }
    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GiftCertificateEntity that = (GiftCertificateEntity) o;
        return Objects.equals(id, that.id) && Objects.equals(tags, that.tags) && Objects.equals(name, that.name) && Objects.equals(description, that.description) && Objects.equals(price, that.price) && Objects.equals(duration, that.duration) && Objects.equals(createDate, that.createDate) && Objects.equals(lastUpdateDate, that.lastUpdateDate);
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {

        return Objects.hash(id, tags, name, description, price, duration, createDate, lastUpdateDate);
    }
}
