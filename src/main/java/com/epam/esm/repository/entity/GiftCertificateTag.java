package com.epam.esm.repository.entity;

import com.fasterxml.jackson.annotation.JsonRootName;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;


/**
 * <p>GiftCertificateTag class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */

@Entity
@Table(name = "gift_certificate_tag")
@JsonRootName(value = "gdf_tag")
public class GiftCertificateTag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "gift_certificate_id")
    private Long giftCertificateId;

    @Column(name = "tag_id")
    private Long tagId;

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Long} object
     */
    public Long getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Long} object
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>giftCertificateId</code>.</p>
     *
     * @return a {@link java.lang.Long} object
     */
    public Long getGiftCertificateId() {
        return giftCertificateId;
    }

    /**
     * <p>Setter for the field <code>giftCertificateId</code>.</p>
     *
     * @param giftCertificateId a {@link java.lang.Long} object
     */
    public void setGiftCertificateId(Long giftCertificateId) {
        this.giftCertificateId = giftCertificateId;
    }

    /**
     * <p>Getter for the field <code>tagId</code>.</p>
     *
     * @return a {@link java.lang.Long} object
     */
    public Long getTagId() {
        return tagId;
    }

    /**
     * <p>Setter for the field <code>tagId</code>.</p>
     *
     * @param tagId a {@link java.lang.Long} object
     */
    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GiftCertificateTag that = (GiftCertificateTag) o;
        return Objects.equals(id, that.id) && Objects.equals(giftCertificateId, that.giftCertificateId) && Objects.equals(tagId, that.tagId);
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {

        return Objects.hash(id, giftCertificateId, tagId);
    }
}
