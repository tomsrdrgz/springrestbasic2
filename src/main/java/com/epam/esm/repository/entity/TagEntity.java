package com.epam.esm.repository.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * <p>TagEntity class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
@Entity
@Table(name = "tag")
@JsonRootName(value = "tag")
public class TagEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;


    @ManyToMany(mappedBy = "tags",cascade = { CascadeType.MERGE }, fetch = FetchType.EAGER)
    @JsonIgnoreProperties("tags")
    private Set<GiftCertificateEntity> giftCertificateEntities = new HashSet<>();

    @Column(name = "name")
    @Size(min = 1, max = 45, message = "Name must be between 1 and 45 characters")
    private String name;

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Long} object
     */
    public Long getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Long} object
     */
    public void setId(Long id) {
        this.id = id;
    }


    /**
     * <p>Getter for the field <code>name</code>.</p>
     *
     * @return a {@link java.lang.String} object
     */
    public String getName() {
        return name;
    }

    /**
     * <p>Setter for the field <code>name</code>.</p>
     *
     * @param name a {@link java.lang.String} object
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * <p>getGiftCertificates.</p>
     *
     * @return a {@link java.util.Set} object
     */
    public Set<GiftCertificateEntity> getGiftCertificates() {
        if(giftCertificateEntities == null) {
            giftCertificateEntities = new HashSet<>();
        }
        return giftCertificateEntities;
    }

    /**
     * <p>setGiftCertificates.</p>
     *
     * @param giftCertificateEntities a {@link java.util.Set} object
     */
    public void setGiftCertificates(Set<GiftCertificateEntity> giftCertificateEntities) {
        this.giftCertificateEntities = giftCertificateEntities;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TagEntity tagEntity = (TagEntity) o;
        return Objects.equals(id, tagEntity.id);
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {

        return Objects.hash(id, name);
    }
}
