package com.epam.esm.repository;

import com.epam.esm.repository.entity.GiftCertificateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <p>GiftCertificateRepository interface.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
@Repository
public interface GiftCertificateRepository extends JpaRepository<GiftCertificateEntity, Long> {


}
