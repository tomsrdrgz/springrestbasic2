package com.epam.esm.repository;

import com.epam.esm.repository.entity.GiftCertificateEntity;
import com.epam.esm.web.dto.SortBy;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * <p>GiftCertificateQueryRepository interface.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
public interface GiftCertificateQueryRepository extends JpaRepository<GiftCertificateEntity, Long> {
    /**
     * <p>findByTagName.</p>
     *
     * @param tagName a {@link java.lang.String} object
     * @param sortBy a {@link com.epam.esm.web.dto.SortBy} object
     * @return a {@link java.util.List} object
     */
    List<GiftCertificateEntity> findByTagName(String tagName, SortBy sortBy);


    /**
     * <p>findByNameOrDescriptionContaining.</p>
     *
     * @param searchString a {@link java.lang.String} object
     * @param sortBy a {@link com.epam.esm.web.dto.SortBy} object
     * @return a {@link java.util.List} object
     */
    List<GiftCertificateEntity> findByNameOrDescriptionContaining(String searchString, SortBy sortBy);

    /**
     * <p>findByTagAndNameOrDescriptionContaining.</p>
     *
     * @param tagName a {@link java.lang.String} object
     * @param searchString a {@link java.lang.String} object
     * @param sortBy a {@link com.epam.esm.web.dto.SortBy} object
     * @return a {@link java.util.List} object
     */
    List<GiftCertificateEntity> findByTagAndNameOrDescriptionContaining(String tagName, String searchString, SortBy sortBy);

}

