package com.epam.esm.service;

import com.epam.esm.repository.GiftCertificateQueryRepositoryImpl;
import com.epam.esm.repository.GiftCertificateRepository;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.repository.entity.GiftCertificateEntity;
import com.epam.esm.repository.entity.TagEntity;
import com.epam.esm.service.mapper.GiftCertificateMapper;
import com.epam.esm.service.mapper.TagMapper;
import com.epam.esm.web.dto.SortBy;
import com.epam.esm.web.dto.GiftCertificateDTO;
import com.epam.esm.web.exception.SpringRestBasicException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import java.util.stream.Collectors;

/**
 * <p>GiftCertificateService class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
@Service
public class GiftCertificateService {

    private final GiftCertificateRepository giftCertificateRepository;
    private final GiftCertificateQueryRepositoryImpl giftCertificateQueryRepositoryImpl;
    private final TagRepository tagRepository;
    private final TagMapper tagMapper = new TagMapper();
    private final GiftCertificateMapper giftCertificateMapper;


    @Autowired
    /**
     * <p>Constructor for GiftCertificateService.</p>
     *
     * @param giftCertificateRepository a {@link com.epam.esm.repository.GiftCertificateRepository} object
     * @param giftCertificateMapper a {@link com.epam.esm.service.mapper.GiftCertificateMapper} object
     * @param tagRepository a {@link com.epam.esm.repository.TagRepository} object
     * @param giftCertificateQueryRepositoryImpl a {@link com.epam.esm.repository.GiftCertificateQueryRepositoryImpl} object
     */
    public GiftCertificateService(GiftCertificateRepository giftCertificateRepository, GiftCertificateMapper giftCertificateMapper,
                                  TagRepository tagRepository,
             GiftCertificateQueryRepositoryImpl giftCertificateQueryRepositoryImpl) {

        this.giftCertificateRepository = giftCertificateRepository;
        this.giftCertificateMapper = giftCertificateMapper;
        this.tagRepository = tagRepository;
        this.giftCertificateQueryRepositoryImpl = giftCertificateQueryRepositoryImpl;
    }


    /**
     * <p>getAllGiftCertificates.</p>
     *
     * @return a {@link java.lang.Iterable} object
     */
    public Iterable<GiftCertificateDTO> getAllGiftCertificates() {

        return giftCertificateRepository.findAll().stream()
                .map(giftCertificateMapper::convertToDTO)
                .collect(Collectors.toList());

    }

    /**
     * <p>getByTagsName.</p>
     *
     * @param tagName a {@link java.lang.String} object
     * @param sortBy  a {@link com.epam.esm.web.dto.SortBy} object
     * @return a {@link java.util.List} object
     */
    public List<GiftCertificateDTO> findByTagName(String tagName, SortBy sortBy) {

        return giftCertificateQueryRepositoryImpl.findByTagName(tagName, sortBy)
                .stream()
                .map(giftCertificateMapper::convertToDTO)
                .collect(Collectors.toList());

    }

    /**
     * <p>findByNameOrDescriptionContaining.</p>
     *
     * @param searchString a {@link java.lang.String} object
     * @param sortBy       a {@link com.epam.esm.web.dto.SortBy} object
     * @return a {@link java.util.List} object
     */
    public List<GiftCertificateDTO> findByNameOrDescriptionContaining(String searchString, SortBy sortBy) {
        return giftCertificateQueryRepositoryImpl.findByNameOrDescriptionContaining(searchString, sortBy)
                .stream()
                .map(giftCertificateMapper::convertToDTO)
                .collect(Collectors.toList());

    }

    /**
     * <p>findByTagAndNameOrDescriptionContaining.</p>
     *
     * @param tagName      a {@link java.lang.String} object
     * @param searchString a {@link java.lang.String} object
     * @param sortBy       a {@link com.epam.esm.web.dto.SortBy} object
     * @return a {@link java.util.List} object
     */
    @Transactional
    public List<GiftCertificateDTO> findByTagAndNameOrDescriptionContaining(String tagName, String searchString, SortBy sortBy) {
        return giftCertificateQueryRepositoryImpl.findByTagAndNameOrDescriptionContaining(tagName, searchString, sortBy)
                .stream()
                .map(giftCertificateMapper::convertToDTO)
                .collect(Collectors.toList());
    }

    /**
     * <p>createGiftCertificate.</p>
     *
     * @param giftCertificateEntity a {@link com.epam.esm.repository.entity.GiftCertificateEntity} object
     * @return a {@link com.epam.esm.web.dto.GiftCertificateDTO} object
     */

    @Transactional
    public GiftCertificateDTO createGiftCertificate(GiftCertificateEntity giftCertificateEntity) {

        giftCertificateEntity.setCreateDate(new Timestamp(DateTime.now().getMillis()));
        giftCertificateEntity.setTags(retrieveTagsToSave(giftCertificateEntity));
        GiftCertificateEntity savedGiftCertificateEntity = giftCertificateRepository.save(giftCertificateEntity);
        return giftCertificateMapper.convertToDTO(savedGiftCertificateEntity);

    }


    /**
     * <p>update GiftCertificate.</p>
     *
     * @param giftCertificateId     a {@link java.lang.Long} object
     * @param giftCertificateEntity a {@link com.epam.esm.repository.entity.GiftCertificateEntity} object
     * @return a {@link com.epam.esm.web.dto.GiftCertificateDTO} object
     */
    @Transactional
    public GiftCertificateDTO updateGiftCertificate(Long giftCertificateId, GiftCertificateEntity giftCertificateEntity) {

        GiftCertificateEntity existingGiftCertificateEntity = giftCertificateRepository.findOne(giftCertificateId);

        if (existingGiftCertificateEntity == null) {
            throw new SpringRestBasicException("GiftCertificate not found with id: " + giftCertificateId, "02");
        }

        Set<TagEntity> tagsToSave = retrieveTagsToSave(giftCertificateEntity);


        // Update the existing giftCertificate properties with the new data
        existingGiftCertificateEntity.setName(giftCertificateEntity.getName());
        existingGiftCertificateEntity.setDescription(giftCertificateEntity.getDescription());
        existingGiftCertificateEntity.setPrice(giftCertificateEntity.getPrice());
        existingGiftCertificateEntity.setDuration(giftCertificateEntity.getDuration());
        existingGiftCertificateEntity.setLastUpdateDate(new Timestamp(DateTime.now().getMillis()));
        existingGiftCertificateEntity.setTags(tagsToSave);

        GiftCertificateEntity updatedGiftCertificateEntity = giftCertificateRepository.save(existingGiftCertificateEntity);
        return giftCertificateMapper.convertToDTO(updatedGiftCertificateEntity);

    }

    /**
     * <p>deleteGiftCertificate.</p>
     *
     * @param giftCertificateId a {@link java.lang.Long} object
     */
    public void deleteGiftCertificate(Long giftCertificateId) {
        GiftCertificateEntity existingGiftCertificateEntity = giftCertificateRepository.findOne(giftCertificateId);
        if (existingGiftCertificateEntity == null) {
            throw new SpringRestBasicException("GiftCertificate not found with id: " + giftCertificateId, "02");
        }

        giftCertificateRepository.delete(existingGiftCertificateEntity);
    }

    /**
     * <p>getOneGiftCertificates.</p>
     *
     * @param giftCertificateId a {@link java.lang.Long} object
     * @return a {@link com.epam.esm.web.dto.GiftCertificateDTO} object
     */
    public GiftCertificateDTO getOneGiftCertificates(Long giftCertificateId) {
        GiftCertificateEntity existingGiftCertificateEntity = giftCertificateRepository.findOne(giftCertificateId);
        if (existingGiftCertificateEntity == null) {
            throw new SpringRestBasicException("Gift Certificate not found with id: " + giftCertificateId, "02");
        }
        return giftCertificateMapper.convertToDTO(existingGiftCertificateEntity);
    }

    /**
     * <p>patchGiftCertificate.</p>
     *
     * @param giftCertificateId     a {@link java.lang.Long} object
     * @param giftCertificateEntity a {@link com.epam.esm.repository.entity.GiftCertificateEntity} object
     * @return a {@link com.epam.esm.web.dto.GiftCertificateDTO} object
     */
    public GiftCertificateDTO patchGiftCertificate(Long giftCertificateId, GiftCertificateEntity giftCertificateEntity) {

        GiftCertificateEntity existingGiftCertificateEntity = giftCertificateRepository.findOne(giftCertificateId);


        if (existingGiftCertificateEntity == null) {
            throw new SpringRestBasicException("GiftCertificate not found with id: " + giftCertificateId, "02");
        }

        if (giftCertificateEntity.getName() != null) {
            existingGiftCertificateEntity.setName(giftCertificateEntity.getName());
        }
        if (giftCertificateEntity.getDescription() != null) {
            existingGiftCertificateEntity.setDescription(giftCertificateEntity.getDescription());
        }
        if (giftCertificateEntity.getPrice() != null) {
            existingGiftCertificateEntity.setPrice(giftCertificateEntity.getPrice());
        }
        if (giftCertificateEntity.getDuration() != null) {
            existingGiftCertificateEntity.setDuration(giftCertificateEntity.getDuration());
        }

        Set<TagEntity> tagsToSave = retrieveTagsToSave(giftCertificateEntity);

        if (giftCertificateEntity.getTags() != null) {
            existingGiftCertificateEntity.setTags(tagsToSave);
        }

        existingGiftCertificateEntity.setLastUpdateDate(new Timestamp(DateTime.now().getMillis()));

        GiftCertificateEntity updatedGiftCertificateEntity = giftCertificateRepository.save(existingGiftCertificateEntity);
        return giftCertificateMapper.convertToDTO(updatedGiftCertificateEntity);

    }
    private Set<TagEntity> retrieveTagsToSave(GiftCertificateEntity giftCertificateEntity) {
        Set<TagEntity> tagsToCheck = new HashSet<>();
        if (giftCertificateEntity.getTags() != null) {
            tagsToCheck = giftCertificateEntity.getTags();
        }

        Set<TagEntity> tagsToSave = new HashSet<>();

        for (TagEntity tag : tagsToCheck) {
            tag.setId(null);
            TagEntity tagCheck = tagRepository.findByName(tag.getName());
            if (tagCheck != null) {
                tag.setId(tagCheck.getId());
                tag.setName(tagCheck.getName());
                tagsToSave.add(tag);
            } else {
                tag.setId(null);
                tagRepository.save(tag);
                tagsToSave.add(tag);
            }
        }
        return tagsToSave;
    }

}
