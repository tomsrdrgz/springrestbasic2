package com.epam.esm.service;

import com.epam.esm.repository.TagRepository;
import com.epam.esm.repository.entity.GiftCertificateEntity;
import com.epam.esm.repository.entity.TagEntity;
import com.epam.esm.service.mapper.TagMapper;
import com.epam.esm.web.dto.TagDTO;
import com.epam.esm.web.exception.SpringRestBasicException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>TagService class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
@Service
public class TagService {

    private final TagRepository tagRepository;
    private final TagMapper tagMapper;


    /**
     * <p>Constructor for TagService.</p>
     *
     * @param tagRepository a {@link com.epam.esm.repository.TagRepository} object
     * @param tagMapper     a {@link com.epam.esm.service.mapper.TagMapper} object
     */
    @Autowired
    public TagService(TagRepository tagRepository, TagMapper tagMapper) {

        this.tagMapper = tagMapper;
        this.tagRepository = tagRepository;

    }

    /**
     * <p>getAllTags.</p>
     *
     * @return a {@link java.util.List} object
     */
    public List<TagDTO> getAllTags() {

        Iterable<TagEntity> tagsEntity = tagRepository.findAll();
        List<TagDTO> tagsDTO = new ArrayList<>();
        for (TagEntity tagEntity : tagsEntity) {
            tagsDTO.add(tagMapper.convertToDTO(tagEntity));
        }
        return tagsDTO;
    }

    /**
     * <p>getTag.</p>
     *
     * @param tagId a {@link java.lang.Long} object
     * @return a {@link com.epam.esm.web.dto.TagDTO} object
     */
    public TagDTO getTag(Long tagId) {

        TagEntity foundTag = tagRepository.findOne(tagId);
        if (foundTag == null) {
            throw new SpringRestBasicException("Could not find Tag with provided ID. " + tagId, "01");
        }
        return tagMapper.convertToDTO(foundTag);
    }

    /**
     * <p>createTag.</p>
     *
     * @param tag a {@link com.epam.esm.repository.entity.TagEntity} object
     * @return a {@link com.epam.esm.web.dto.TagDTO} object
     */
    public TagDTO createTag(TagEntity tag) {

        TagEntity existingTag = tagRepository.findByName(tag.getName());
        if (existingTag != null) {

            throw new SpringRestBasicException("Tag with name " + tag.getName() + " already exist", "01");

        }
        return tagMapper.convertToDTO(tagRepository.save(tag));
    }


    /**
     * <p>deleteTag.</p>
     *
     * @param tagId a {@link java.lang.Long} object
     */
    public void deleteTag(Long tagId) {

        TagEntity existingTag = tagRepository.findOne(tagId);
        if (existingTag == null) {
            throw new SpringRestBasicException("Tag could not be found in Database with ID. " + tagId, "01");
        }

        if (!existingTag.getGiftCertificates().isEmpty()) {
            List<GiftCertificateEntity> listOfCertificates = new ArrayList<>(existingTag.getGiftCertificates());
            for (GiftCertificateEntity giftCertificateEntity : listOfCertificates) {
                giftCertificateEntity.getTags().remove(existingTag);
            }
        }

        tagRepository.delete(existingTag);

    }

}
