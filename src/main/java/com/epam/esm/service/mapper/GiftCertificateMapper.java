package com.epam.esm.service.mapper;

import com.epam.esm.repository.entity.GiftCertificateEntity;
import com.epam.esm.web.dto.GiftCertificateDTO;
import org.springframework.stereotype.Component;

/**
 * <p>GiftCertificateMapper class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
@Component
public class GiftCertificateMapper {


    /**
     * <p>convertToDTO.</p>
     *
     * @param giftCertificateEntity a {@link com.epam.esm.repository.entity.GiftCertificateEntity} object
     * @return a {@link com.epam.esm.web.dto.GiftCertificateDTO} object
     */
    public GiftCertificateDTO convertToDTO(GiftCertificateEntity giftCertificateEntity) {

        GiftCertificateDTO giftCertificateDTO = new GiftCertificateDTO();
        giftCertificateDTO.setId(giftCertificateEntity.getId());
        giftCertificateDTO.setName(giftCertificateEntity.getName());
        giftCertificateDTO.setDescription(giftCertificateEntity.getDescription());
        giftCertificateDTO.setPrice(giftCertificateEntity.getPrice());
        giftCertificateDTO.setDuration(giftCertificateEntity.getDuration());
        giftCertificateDTO.setCreateDate(giftCertificateEntity.getCreateDate());
        giftCertificateDTO.setLastUpdateDate(giftCertificateEntity.getLastUpdateDate());
        giftCertificateDTO.setTags(giftCertificateEntity.getTags());
        return giftCertificateDTO;
    }

    /**
     * <p>convertFromDTO.</p>
     *
     * @param giftCertificateDTO a {@link com.epam.esm.web.dto.GiftCertificateDTO} object
     * @return a {@link com.epam.esm.repository.entity.GiftCertificateEntity} object
     */
    public GiftCertificateEntity convertFromDTO(GiftCertificateDTO giftCertificateDTO){

        GiftCertificateEntity giftCertificateEntity = new GiftCertificateEntity();
        giftCertificateEntity.setId(giftCertificateDTO.getId());
        giftCertificateEntity.setName(giftCertificateDTO.getName());
        giftCertificateEntity.setDescription(giftCertificateDTO.getDescription());
        giftCertificateEntity.setPrice(giftCertificateDTO.getPrice());
        giftCertificateEntity.setDuration(giftCertificateDTO.getDuration());
        giftCertificateEntity.setCreateDate(giftCertificateDTO.getCreateDate());
        giftCertificateEntity.setLastUpdateDate(giftCertificateDTO.getLastUpdateDate());
        giftCertificateEntity.setTags(giftCertificateDTO.getTags());
        return giftCertificateEntity;
    }
}
