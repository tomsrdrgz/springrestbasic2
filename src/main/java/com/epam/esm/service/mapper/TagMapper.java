package com.epam.esm.service.mapper;

import com.epam.esm.repository.entity.TagEntity;
import com.epam.esm.web.dto.TagDTO;
import org.springframework.stereotype.Component;

/**
 * <p>TagMapper class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
@Component
public class TagMapper {

    /**
     * <p>convertToDTO.</p>
     *
     * @param tagEntity a {@link com.epam.esm.repository.entity.TagEntity} object
     * @return a {@link com.epam.esm.web.dto.TagDTO} object
     */
    public TagDTO convertToDTO(TagEntity tagEntity) {

        TagDTO tagDTO = new TagDTO();
        tagDTO.setId(tagEntity.getId());
        tagDTO.setName(tagEntity.getName());
        tagDTO.setGiftCertificates(tagEntity.getGiftCertificates());

        return tagDTO;
    }

    /**
     * <p>convertFromDTO.</p>
     *
     * @param tagDTO a {@link com.epam.esm.web.dto.TagDTO} object
     * @return a {@link com.epam.esm.repository.entity.TagEntity} object
     */
    public TagEntity convertFromDTO(TagDTO tagDTO) {

        TagEntity tagEntity = new TagEntity();
        tagEntity.setId(tagDTO.getId());
        tagEntity.setName(tagDTO.getName());
        tagEntity.setGiftCertificates(tagDTO.getGiftCertificates());
        return tagEntity;
    }

}
