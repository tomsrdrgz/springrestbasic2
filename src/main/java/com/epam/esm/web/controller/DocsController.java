package com.epam.esm.web.controller;

import io.swagger.annotations.Api;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * <p>DocsController class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
@Controller
@RequestMapping("/null/swagger-resources/configuration/ui")
public class DocsController {


    /**
     * <p>swaggerUi.</p>
     *
     * @return a {@link java.lang.String} object
     */
    @GetMapping("")
    public String swaggerUi() {
        // Your logic here
        return "redirect:/swagger-ui.html";
    }
    /**
     * <p>api.</p>
     *
     * @return a {@link springfox.documentation.spring.web.plugins.Docket} object
     */
    @Bean
    public Docket api() {

        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                .paths(PathSelectors.any()).build().pathMapping("")
                .apiInfo(apiInfo()).useDefaultResponseMessages(false);
    }

    @Bean
    ApiInfo apiInfo() {

        final ApiInfoBuilder builder = new ApiInfoBuilder();
        builder.title("Epam REST Spring Task 1").version("1.0").license("(C)Copyright EPAM")
                .description("List of all endpoints used in API");
        return builder.build();
    }
}
