package com.epam.esm.web.controller;

import com.epam.esm.service.GiftCertificateService;
import com.epam.esm.service.mapper.GiftCertificateMapper;
import com.epam.esm.web.dto.GiftCertificateDTO;
import com.epam.esm.web.dto.SortBy;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


/**
 * <p>GiftCertificateController class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
@RestController
@RequestMapping("/giftCertificates")
@Api(value = "Gift Certificates")
public class GiftCertificateController {


    private final GiftCertificateService giftCertificateService;

    private final GiftCertificateMapper giftCertificateMapper;


    /**
     * <p>Constructor for GiftCertificateController.</p>
     *
     * @param giftCertificateService a {@link com.epam.esm.service.GiftCertificateService} object
     * @param giftCertificateMapper  a {@link com.epam.esm.service.mapper.GiftCertificateMapper} object
     */
    @Autowired
    public GiftCertificateController(GiftCertificateService giftCertificateService, GiftCertificateMapper giftCertificateMapper) {
        this.giftCertificateMapper = giftCertificateMapper;
        this.giftCertificateService = giftCertificateService;
    }


    /**
     * Retrieves a list of all available Gift Certificates.
     *
     * @param headers HttpHeaders from the request
     * @return ResponseEntity containing a list of GiftCertificateDTOs
     */

    @ApiOperation(value = "View a list of available Gift Certificates", response = Iterable.class)
    @GetMapping(value = "", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Iterable<GiftCertificateDTO>> getAllGiftCertificates(@RequestHeader HttpHeaders headers) {
        return new ResponseEntity<>(giftCertificateService.getAllGiftCertificates(), headers, HttpStatus.OK);
    }

    /**
     * Retrieves a specific Gift Certificate by its ID.
     *
     * @param giftCertificateId Path variable representing the ID of the Gift Certificate
     * @param headers           HttpHeaders from the request
     * @return ResponseEntity containing the requested GiftCertificateDTO
     */


    @ApiOperation(value = "View a list of available Gift Certificates", response = Iterable.class)
    @GetMapping(value = "/{giftCertificateId}",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<GiftCertificateDTO> findGiftCertificateById(@PathVariable Long giftCertificateId,
                                                                      @RequestHeader HttpHeaders headers) {
        return new ResponseEntity<>(giftCertificateService.getOneGiftCertificates(giftCertificateId), headers, HttpStatus.OK);
    }

    /**
     * Searches for Gift Certificates based on tag name and/or a search string contained by name or description of GiftCertificate.
     *
     * @param tagName Name of the tag to search for
     * @param sortBy  SortBy enum for sorting the result
     * @param headers HttpHeaders from the request
     * @return ResponseEntity containing a list of matching GiftCertificateDTOs
     * @param certificateNameOrDescriptionContains a {@link java.lang.String} object
     */


    @GetMapping(value = "/search", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<GiftCertificateDTO>> findTagsBySearch(@RequestParam(required = false) String tagName,
                                                                     @RequestParam(required = false) String certificateNameOrDescriptionContains,
                                                                     @RequestParam(required = false, defaultValue = "DATE_ASC") SortBy sortBy,
                                                                     @RequestHeader HttpHeaders headers) {
        List<GiftCertificateDTO> giftCertificates ;

        if (tagName != null && certificateNameOrDescriptionContains != null) {
            giftCertificates = giftCertificateService.findByTagAndNameOrDescriptionContaining
                    (tagName, certificateNameOrDescriptionContains, sortBy);
        } else if (tagName != null) {
            giftCertificates = giftCertificateService.findByTagName(tagName, sortBy);
        } else if (certificateNameOrDescriptionContains != null) {
            giftCertificates = giftCertificateService.findByNameOrDescriptionContaining(certificateNameOrDescriptionContains, sortBy);
        } else {
            giftCertificates = StreamSupport.stream(giftCertificateService.getAllGiftCertificates().spliterator(), false)
                    .collect(Collectors.toList());
        }

        return new ResponseEntity<>(giftCertificates, headers, HttpStatus.OK);

    }

    /**
     * Creates a new Gift Certificate.
     *
     * @param giftCertificateDTO The GiftCertificateDTO to be created
     * @return ResponseEntity containing the created GiftCertificateDTO
     */


    @ApiOperation(value = "Create a new Gift Certificate", response = GiftCertificateDTO.class)
    @PostMapping(value = "", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<GiftCertificateDTO> createGiftCertificate(@RequestBody GiftCertificateDTO
                                                                            giftCertificateDTO) {

        return new ResponseEntity<>(giftCertificateService.
                createGiftCertificate(giftCertificateMapper.convertFromDTO(giftCertificateDTO)), HttpStatus.CREATED);
    }


    /**
     * Updates an existing Gift Certificate.
     *
     * @param giftCertificateId  Path variable representing the ID of the Gift Certificate
     * @param giftCertificateDTO The updated GiftCertificateDTO
     * @return ResponseEntity containing the updated GiftCertificateDTO
     */


    @ApiOperation(value = "Update an existing Gift Certificate", response = GiftCertificateDTO.class)
    @PutMapping(value = "/{giftCertificateId}", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<GiftCertificateDTO> updateGiftCertificate(@PathVariable Long giftCertificateId,
                                                                    @RequestBody GiftCertificateDTO giftCertificateDTO) {

        return new ResponseEntity<>(giftCertificateService
                .updateGiftCertificate(giftCertificateId, giftCertificateMapper.convertFromDTO(giftCertificateDTO)), HttpStatus.OK);
    }


    /**
     * Updates an existing Gift Certificate using partial updates (PATCH).
     *
     * @param giftCertificateId  Path variable representing the ID of the Gift Certificate
     * @param giftCertificateDTO The partially updated GiftCertificateDTO
     * @return ResponseEntity containing the partially updated GiftCertificateDTO
     */

    @ApiOperation(value = "Update an existing Gift Certificate", response = GiftCertificateDTO.class)
    @PatchMapping(value = "/{giftCertificateId}", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<GiftCertificateDTO> updateGiftCertificatePatch(@PathVariable Long giftCertificateId,
                                                                         @RequestBody GiftCertificateDTO giftCertificateDTO) {

        return new ResponseEntity<>(giftCertificateService
                .patchGiftCertificate(giftCertificateId, giftCertificateMapper.convertFromDTO(giftCertificateDTO)), HttpStatus.OK);
    }


    /**
     * Deletes an existing Gift Certificate.
     *
     * @param giftCertificateId Path variable representing the ID of the Gift Certificate to be deleted
     * @return ResponseEntity with HttpStatus.NO_CONTENT
     */


    @ApiOperation(value = "Delete an existing Gift Certificate")
    @DeleteMapping(value = "/{giftCertificateId}")
    public ResponseEntity<GiftCertificateDTO> deleteGiftCertificate(@PathVariable Long giftCertificateId) {
        giftCertificateService.deleteGiftCertificate(giftCertificateId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
