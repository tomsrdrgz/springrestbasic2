package com.epam.esm.web.controller;


import com.epam.esm.repository.entity.TagEntity;
import com.epam.esm.service.TagService;
import com.epam.esm.web.dto.TagDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * <p>TagController class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
@RestController
@RequestMapping("/tags")
@Api(value = "Tags")
public class TagController {

    private final TagService tagService;

    /**
     * <p>Constructor for TagController.</p>
     *
     * @param tagService a {@link com.epam.esm.service.TagService} object
     */
    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    /**
     * Retrieves a list of all available Tags.
     *
     * @return ResponseEntity containing a list of TagDTOs
     */

    @ApiOperation(value = "View a list of available Tags", response = Iterable.class)
    @GetMapping(value = "",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Iterable<TagDTO>> findAllTags() {
        return new ResponseEntity<>(tagService.getAllTags(), HttpStatus.OK);
    }


    /**
     * Retrieves a specific Tag by its ID.
     *
     * @param tagId Path variable representing the ID of the Tag
     * @return ResponseEntity containing the requested TagDTO
     */


    @ApiOperation(value = "View a Tag with specific Id", response = Iterable.class)
    @GetMapping(value = "/{tagId}",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<TagDTO> getTag(@PathVariable Long tagId) {
        return new ResponseEntity<>(tagService.getTag(tagId), HttpStatus.OK);
    }

    /**
     * Creates a new Tag. Tags are unique by name.
     *
     * @param tag The TagEntity to be created
     * @return ResponseEntity containing the created TagDTO
     */


    @ApiOperation(value = "Create a new Tag", response = TagEntity.class)
    @PostMapping(value = "",
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<TagDTO> createTag(@RequestBody TagEntity tag) {
        return new ResponseEntity<>(tagService.createTag(tag), HttpStatus.CREATED);
    }


    /**
     * Deletes an existing Tag by its ID.
     *
     * @param tagId Path variable representing the ID of the Tag to be deleted
     * @return ResponseEntity with HttpStatus.NO_CONTENT
     */

    @ApiOperation(value = "Delete an existing Tag")
    @DeleteMapping(value = "/{tagId}")
    public ResponseEntity<TagDTO> deleteTag(@PathVariable Long tagId) {

        tagService.deleteTag(tagId);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
