package com.epam.esm.web.exception;

import com.epam.esm.web.dto.ErrorResponse;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import org.apache.commons.lang3.text.StrBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;

/**
 * <p>SpringRestBasicExceptionHandler class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */

@ControllerAdvice
public class SpringRestBasicExceptionHandler {
    
    /**
     * <p>handleException.</p>
     *
     * @param e a {@link com.epam.esm.web.exception.SpringRestBasicException} object
     * @return a {@link org.springframework.http.ResponseEntity} object
     */


    @ExceptionHandler(SpringRestBasicException.class)
    public  ResponseEntity<ErrorResponse> handleException(SpringRestBasicException e) {
        return new ResponseEntity<>(new ErrorResponse(e.getMessage(), HttpStatus.NOT_FOUND.value() + e.getCode()), HttpStatus.NOT_FOUND);
    }


    /**
     * <p>handleConstraintViolationException.</p>
     *
     * @param e a {@link javax.validation.ConstraintViolationException} object
     * @return a {@link org.springframework.http.ResponseEntity} object
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public  ResponseEntity<ErrorResponse>  handleConstraintViolationException(ConstraintViolationException e) {
        StringBuilder errors = new StringBuilder();
        e.getConstraintViolations().forEach(error-> errors.append(error.getMessageTemplate()).append(". /n    "));//TODO
        return new ResponseEntity<>(new ErrorResponse(errors.toString()), HttpStatus.NOT_FOUND);
    }


    /**
     * <p>handleUnrecognizedPropertyException.</p>
     *
     * @param e a {@link com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException} object
     * @return a {@link org.springframework.http.ResponseEntity} object
     */
    @ExceptionHandler(UnrecognizedPropertyException.class)
    public ResponseEntity<ErrorResponse> handleUnrecognizedPropertyException(UnrecognizedPropertyException e) {
        return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
    }

}
