package com.epam.esm.web.exception;



/**
 * <p>SpringRestBasicException class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
public class SpringRestBasicException extends RuntimeException {
    private static String message;
    private static String code;

    /**
     * <p>Constructor for SpringRestBasicException.</p>
     *
     * @param message a {@link java.lang.String} object
     * @param code a {@link java.lang.Integer} object
     */
    public SpringRestBasicException(String message, String code) {

        SpringRestBasicException.message = message;
        SpringRestBasicException.code = code;
    }

    /**
     * <p>Getter for the field <code>message</code>.</p>
     *
     * @return a {@link java.lang.String} object
     */
    public String getMessage() {

        return message;
    }

    /**
     * <p>Setter for the field <code>message</code>.</p>
     *
     * @param message a {@link java.lang.String} object
     */
    public  void setMessage(String message) {

        SpringRestBasicException.message = message;
    }

    /**
     * <p>Getter for the field <code>code</code>.</p>
     *
     * @return a {@link java.lang.Integer} object
     */
    public  String getCode() {

        return code;
    }


    /**
     * <p>Setter for the field <code>code</code>.</p>
     *
     * @param code a {@link java.lang.Integer} object
     */
    public void setCode(String code) {

        SpringRestBasicException.code = code;
    }
}
