package com.epam.esm.web.dto;

import com.epam.esm.repository.entity.GiftCertificateEntity;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;

import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.Set;

/**
 * <p>TagDTO class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
@JsonRootName(value = "tag")
public class TagDTO {
    @JsonAlias("id")
    private Long id;

    @JsonAlias("name")
    @Size(min = 1, max = 45, message = "Name must be between 1 and 45 characters")
    private String name;

    @JsonAlias("giftCertificates")
    @JsonIgnoreProperties("tags")
    private Set<GiftCertificateEntity> giftCertificateEntities;

    /**
     * <p>getGiftCertificates.</p>
     *
     * @return a {@link java.util.Set} object
     */
    public Set<GiftCertificateEntity> getGiftCertificates() {
        return giftCertificateEntities;
    }

    /**
     * <p>setGiftCertificates.</p>
     *
     * @param giftCertificateEntities a {@link java.util.Set} object
     */
    public void setGiftCertificates(Set<GiftCertificateEntity> giftCertificateEntities) {
        this.giftCertificateEntities = giftCertificateEntities;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Long} object
     */
    public Long getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Long} object
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>name</code>.</p>
     *
     * @return a {@link java.lang.String} object
     */
    public String getName() {
        return name;
    }

    /**
     * <p>Setter for the field <code>name</code>.</p>
     *
     * @param name a {@link java.lang.String} object
     */
    public void setName(String name) {
        this.name = name;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TagDTO tagDTO = (TagDTO) o;
        return Objects.equals(id, tagDTO.id) && Objects.equals(name, tagDTO.name);
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}

