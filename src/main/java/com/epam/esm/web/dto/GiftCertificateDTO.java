package com.epam.esm.web.dto;

import com.epam.esm.repository.entity.TagEntity;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.Set;

/**
 * <p>GiftCertificateDTO class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
@JsonRootName(value = "gift_certificate_DTO")
public class GiftCertificateDTO {

    @JsonAlias("id")
    private Long id;
    @JsonAlias("name")
    @NotNull(message = "Name cannot be null")
    @Size(min = 1, max = 45, message = "Name must be between 1 and 45 characters")
    private String name;
    @JsonAlias("description")
    @Size(min = 1, max = 45, message = "Description must be between 1 and 45 characters")
    private String description;
    @JsonAlias("price")
    @Min(value = 0, message = "Price must be greater than 0.01")
    @Max(value = 999999, message = "Price must be less than 999999.99")
    private BigDecimal price;
    @JsonAlias("duration")
    @Min(value = 1, message = "Duration must be greater than 0")
    @Max(value = 999999, message = "Duration must be less than 999999")
    private Integer duration;


    @JsonAlias("create_date")
    private Timestamp createDate;

    @JsonAlias("last_update_date")
    private Timestamp lastUpdateDate;

    @JsonAlias("tags")
    @JsonIgnoreProperties("giftCertificates")
    private Set<TagEntity> tags;

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Long} object
     */
    public Long getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.Long} object
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>name</code>.</p>
     *
     * @return a {@link java.lang.String} object
     */
    public String getName() {
        return name;
    }

    /**
     * <p>Setter for the field <code>name</code>.</p>
     *
     * @param name a {@link java.lang.String} object
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the field <code>description</code>.</p>
     *
     * @return a {@link java.lang.String} object
     */
    public String getDescription() {
        return description;
    }

    /**
     * <p>Setter for the field <code>description</code>.</p>
     *
     * @param description a {@link java.lang.String} object
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * <p>Getter for the field <code>price</code>.</p>
     *
     * @return a {@link java.math.BigDecimal} object
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * <p>Setter for the field <code>price</code>.</p>
     *
     * @param price a {@link java.math.BigDecimal} object
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * <p>Getter for the field <code>duration</code>.</p>
     *
     * @return a {@link java.lang.Integer} object
     */
    public Integer getDuration() {
        return duration;
    }

    /**
     * <p>Setter for the field <code>duration</code>.</p>
     *
     * @param duration a {@link java.lang.Integer} object
     */
    public void setDuration(Integer duration) {
        this.duration = duration;
    }


    /**
     * <p>Getter for the field <code>tags</code>.</p>
     *
     * @return a {@link java.util.Set} object
     */
    public Set<TagEntity> getTags() {
        return tags;
    }

    /**
     * <p>Setter for the field <code>tags</code>.</p>
     *
     * @param tags a {@link java.util.Set} object
     */
    public void setTags(Set<TagEntity> tags) {
        this.tags = tags;
    }

    /**
     * <p>Getter for the field <code>createDate</code>.</p>
     *
     * @return a Timestamp object
     */
    public Timestamp getCreateDate() {
        return createDate;
    }

/**
 * <p>Setter for the field <code>createDate</code>.</p>
 *
 * @param createDate a Timestamp object
 */
public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    /**
     * <p>Getter for the field <code>lastUpdateDate</code>.</p>
     *
     * @return a Timestamp object
     */
    public Timestamp getLastUpdateDate() {
        return lastUpdateDate;
    }

    /**
     * <p>Setter for the field <code>lastUpdateDate</code>.</p>
     *
     * @param last_update_date a Timestamp object
     */
    public void setLastUpdateDate(Timestamp last_update_date) {
        this.lastUpdateDate = last_update_date;
    }


    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GiftCertificateDTO that = (GiftCertificateDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(description, that.description) && Objects.equals(price, that.price) && Objects.equals(duration, that.duration) && Objects.equals(createDate, that.createDate) && Objects.equals(lastUpdateDate, that.lastUpdateDate);
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, price, duration, createDate, lastUpdateDate);
    }
}
