package com.epam.esm.web.dto;

/**
 * <p>SortBy class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
public enum SortBy {
    DATE_ASC,
    DATE_DESC,
    NAME_ASC,
    NAME_DESC
}
