package com.epam.esm.web.dto;

/**
 * <p>ErrorResponse class.</p>
 *
 * @author Tomas_Rodriguez
 * @version $Id: $Id
 */
public class ErrorResponse {
    private String message;
    private String errorCode;

    /**
     * <p>Constructor for ErrorResponse.</p>
     *
     * @param message a {@link java.lang.String} object
     */
    public ErrorResponse(String message) {
        this.message = message;
    }



    /**
     * <p>Constructor for ErrorResponse.</p>
     *
     * @param message a {@link java.lang.String} object
     * @param errorCode a {@link java.lang.String} object
     */
    public ErrorResponse(String message, String errorCode) {
        this.message = message;
        this.errorCode = errorCode;
    }
    /**
     * <p>Getter for the field <code>message</code>.</p>
     *
     * @return a {@link java.lang.String} object
     */
    public String getMessage() {
        return message;
    }

    /**
     * <p>Setter for the field <code>message</code>.</p>
     *
     * @param message a {@link java.lang.String} object
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * <p>Getter for the field <code>errorCode</code>.</p>
     *
     * @return a {@link java.lang.String} object
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * <p>Setter for the field <code>errorCode</code>.</p>
     *
     * @param errorCode a {@link java.lang.String} object
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

}
