insert into gift_certificate values (1,
'Gift C 1','Gift C 1',10.00,10,'2018-09-02 16:00:01','2018-09-02 16:00:01');
insert into gift_certificate values (2,
 'Gift C 2','Gift C 2',20.00,20,'2018-09-02 16:00:02','2018-09-02 16:00:02');
insert into gift_certificate values (3,
'Gift C 3','Gift C 3',30.00,30,'2018-09-02 16:00:03','2018-09-02 16:00:03');

insert into tag values (1,'Tag 1');
insert into tag values (2,'Tag 2');
insert into tag values (3,'Tag 3');

insert into gift_certificate_tag values (1, 2);
insert into gift_certificate_tag values (2, 3);