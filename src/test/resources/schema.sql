create table gift_certificate (
  id  bigint identity primary key,
  name varchar(255) not null,
  description varchar(255) not null,
  price decimal(19,2) not null,
  duration int not null,
  create_date timestamp,
  last_update_date timestamp
);


create table tag (
 id bigint identity primary key,
 name varchar(255) not null,
);
CREATE TABLE GIFT_CERTIFICATE_TAG (
  GIFT_CERTIFICATE_ID bigint NOT NULL,
  TAG_ID bigint NOT NULL,
  PRIMARY KEY (GIFT_CERTIFICATE_ID, TAG_ID),
  FOREIGN KEY (GIFT_CERTIFICATE_ID) REFERENCES GIFT_CERTIFICATE(ID),
  FOREIGN KEY (TAG_ID) REFERENCES TAG(ID)
);