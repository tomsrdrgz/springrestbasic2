package com.epam.esm.service;

import com.epam.esm.repository.GiftCertificateRepository;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.repository.entity.GiftCertificateEntity;
import com.epam.esm.repository.GiftCertificateQueryRepositoryImpl;
import com.epam.esm.repository.entity.TagEntity;
import com.epam.esm.service.mapper.GiftCertificateMapper;
import com.epam.esm.web.dto.SortBy;
import com.epam.esm.web.dto.GiftCertificateDTO;
import com.epam.esm.web.exception.SpringRestBasicException;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.*;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GiftCertificateServiceTest {
    @Mock
    private GiftCertificateRepository giftCertificateRepository;

    @Mock
    private GiftCertificateQueryRepositoryImpl giftCertificateQueryRepositoryImpl;
    @Mock
    private TagRepository tagRepository;
    private final GiftCertificateMapper giftCertificateMapper = new GiftCertificateMapper();

    @InjectMocks
    private GiftCertificateService giftCertificateService;
    private List<GiftCertificateEntity> giftCertificates;
    private List<GiftCertificateEntity> giftCertificates2;


    @Before
    public void setUp() {

        giftCertificateService = new GiftCertificateService(giftCertificateRepository, giftCertificateMapper, tagRepository, giftCertificateQueryRepositoryImpl);
        giftCertificates = new ArrayList<>();
        giftCertificates2 = new ArrayList<>();
        GiftCertificateEntity giftCertificate = new GiftCertificateEntity();

        giftCertificate.setId(1L);
        giftCertificate.setName("aa");
        giftCertificate.setDescription("bb");
        giftCertificate.setPrice(BigDecimal.valueOf(10));
        giftCertificate.setDuration(1);
        giftCertificate.setCreateDate(null);
        giftCertificate.setLastUpdateDate(null);


        GiftCertificateEntity giftCertificate2 = new GiftCertificateEntity();

        giftCertificate2.setId(2L);
        giftCertificate2.setName("bb");
        giftCertificate2.setDescription("bb");
        giftCertificate2.setPrice(BigDecimal.valueOf(20));
        giftCertificate2.setDuration(1);
        giftCertificate2.setCreateDate(null);
        giftCertificate2.setLastUpdateDate(null);

        GiftCertificateEntity giftCertificate3 = new GiftCertificateEntity();
        giftCertificate3.setId(3L);
        giftCertificate3.setName("ccaa");
        giftCertificate3.setDescription("bb");
        giftCertificate3.setPrice(BigDecimal.valueOf(30));
        giftCertificate3.setDuration(1);
        giftCertificate3.setCreateDate(null);
        giftCertificate3.setLastUpdateDate(null);

        giftCertificates.add(giftCertificate);
        giftCertificates.add(giftCertificate2);
        giftCertificates.add(giftCertificate3);

        when(giftCertificateRepository.findAll()).thenReturn(giftCertificates);
        when(giftCertificateRepository.findOne(1L)).thenReturn(giftCertificate);

        when(giftCertificateRepository.findOne(4L)).thenReturn(null);

        giftCertificates2.add(giftCertificate);
        giftCertificates2.add(giftCertificate3);

        List<GiftCertificateEntity> Listtag = new ArrayList<>();
        Listtag.add(giftCertificate);

    }

    @Test
    public void getByTagsNameTest() {

        when(giftCertificateQueryRepositoryImpl.findByTagName("aa", SortBy.DATE_ASC)).thenReturn(giftCertificates2);
        Iterable<GiftCertificateDTO> actualResult = giftCertificateService.findByTagName("aa", SortBy.DATE_ASC);
        Iterable<GiftCertificateDTO> expectedResult = createDTOList2();
        Assertions.assertThat(actualResult).usingRecursiveComparison().isEqualTo(expectedResult);

    }

    @Test
    public void getByTagsNameFailTest() {

        //      when(customGiftCertificateRepositoryImpl.findByTagName("ll", SortBy.DATE_ASC)).thenReturn(null);
        Iterable<GiftCertificateDTO> actualResult = giftCertificateService.findByTagName("ll", SortBy.NAME_DESC);
        Iterable<GiftCertificateDTO> expectedResult = new ArrayList<>();
        Assertions.assertThat(actualResult).usingRecursiveComparison().isEqualTo(expectedResult);

    }

    @Test
    public void getAllGiftCertificates() {
        Iterable<GiftCertificateDTO> actualResult = giftCertificateService.getAllGiftCertificates();
        Iterable<GiftCertificateDTO> expectedResult = createDTOList();
        Assertions.assertThat(actualResult).usingRecursiveComparison().isEqualTo(expectedResult);
    }


    @Test
    public void findByNameOrDescriptionContainingTest2() {

        Iterable<GiftCertificateDTO> actualResult = giftCertificateService.findByNameOrDescriptionContaining("gh", SortBy.NAME_DESC);
        Iterable<GiftCertificateDTO> expectedResult = new ArrayList<>();
        Assertions.assertThat(actualResult).usingRecursiveComparison().isEqualTo(expectedResult);
    }


    @Test
    public void findByTagNameOrDescriptionContainingTest2() {
        // when(giftCertificateRepository.findByTagAndNameOrDescriptionContaining("gh","gh")).thenReturn(null);
        Iterable<GiftCertificateDTO> actualResult = giftCertificateService.findByTagAndNameOrDescriptionContaining("Tag 1", "gh", SortBy.NAME_DESC);
        Iterable<GiftCertificateDTO> expectedResult = new ArrayList<>();
        Assertions.assertThat(actualResult).usingRecursiveComparison().isEqualTo(expectedResult);
    }


    @Test
    public void getOneGiftCertificates() {
        GiftCertificateDTO actualResult = giftCertificateService.getOneGiftCertificates(1L);
        GiftCertificateDTO expectedResult = createDTO();
        Assertions.assertThat(actualResult).usingRecursiveComparison().isEqualTo(expectedResult);
    }

    @Test
    public void getOneCertificateNotfoundTest() {
        try {
            giftCertificateService.getOneGiftCertificates(4L);
        } catch (SpringRestBasicException e) {
            Assertions.assertThat(e.getMessage()).isEqualTo("Gift Certificate not found with id: " + 4L);
        }
    }

    @Test
    public void createGiftCertificateTest() {
        GiftCertificateEntity giftCertificateNew = giftCertificateNew3();
        when(giftCertificateRepository.save(giftCertificateNew)).thenReturn(giftCertificateNew);
        GiftCertificateDTO actualResult = giftCertificateService.createGiftCertificate(giftCertificateNew);
        GiftCertificateDTO expectedResult = createDTO2();
        Assertions.assertThat(actualResult).usingRecursiveComparison().ignoringFields("createDate", "lastUpdateDate").isEqualTo(expectedResult);

    }


    @Test
    public void createGiftCertificateWithTagsTest() {

        GiftCertificateEntity giftCertificateNew = giftCertificateNew();
        TagEntity tag = new TagEntity();
        tag.setName("Tag 7");
        tag.setId(7L);
        Set<TagEntity> tags = new HashSet<>();
        tags.add(tag);
        giftCertificateNew.setTags(tags);

        when(giftCertificateRepository.save(giftCertificateNew)).thenReturn(giftCertificateNew);
        when(tagRepository.findByName("Tag 7")).thenReturn(null);
        GiftCertificateDTO actualResult = giftCertificateService.createGiftCertificate(giftCertificateNew);
        GiftCertificateDTO expectedResult = createDTO3();
        Set<TagEntity> actualTags;
        actualTags = giftCertificateNew.getTags();
        Set<TagEntity> expectedTags = expectedResult.getTags();
        Assertions.assertThat(actualResult).usingRecursiveComparison().ignoringFields("tags", "createDate", "lastUpdateDate").isEqualTo(expectedResult);
        Assertions.assertThat(actualTags.toArray()).usingRecursiveComparison().ignoringFields("id").isEqualTo(expectedTags.toArray());
    }

    @Test
    public void createGiftCertificateWithTagsAlreadyExistTest() {


        GiftCertificateEntity giftCertificateNew = giftCertificateNew();
        TagEntity tag = new TagEntity();
        tag.setName("Tag 7");
        tag.setId(7L);
        Set<TagEntity> tags = new HashSet<>();
        tags.add(tag);
        giftCertificateNew.setTags(tags);

        when(giftCertificateRepository.save(giftCertificateNew)).thenReturn(giftCertificateNew);
        when(tagRepository.findByName("Tag 7")).thenReturn(tag);
        GiftCertificateDTO actualResult = giftCertificateService.createGiftCertificate(giftCertificateNew);
        GiftCertificateDTO expectedResult = createDTO3();
        Set<TagEntity> actualTags;
        actualTags = giftCertificateNew.getTags();
        Set<TagEntity> expectedTags = expectedResult.getTags();
        Assertions.assertThat(actualResult).usingRecursiveComparison().ignoringFields("tags", "createDate", "lastUpdateDate").isEqualTo(expectedResult);
        Assertions.assertThat(actualTags.toArray()).usingRecursiveComparison().ignoringFields("id").isEqualTo(expectedTags.toArray());
    }

    @Test
    public void updateGiftCertificateTest() {

        GiftCertificateEntity beforeEntity = giftCertificateNew();

        GiftCertificateEntity afterEntity = giftCertificateNew2();

        when(giftCertificateRepository.findOne(4L)).thenReturn(beforeEntity);
        when(giftCertificateRepository.save(beforeEntity)).thenReturn(afterEntity);
        GiftCertificateDTO actualResult = giftCertificateService.updateGiftCertificate(4L, beforeEntity);
        GiftCertificateDTO expectedResult = createDTO3();
        Set<TagEntity> actualTags;
        actualTags = afterEntity.getTags();
        Set<TagEntity> expectedTags = expectedResult.getTags();
        Assertions.assertThat(actualResult).usingRecursiveComparison().ignoringFields("createDate", "lastUpdateDate").isEqualTo(expectedResult);
        Assertions.assertThat(actualTags.toArray()).usingRecursiveComparison().ignoringFields("id").isEqualTo(expectedTags.toArray());
    }

    @Test
    public void updateGiftCertificateAlreadyFoundTagNameTest() {

        GiftCertificateEntity beforeEntity = giftCertificateNew();

        GiftCertificateEntity afterEntity = giftCertificateNew2();
        when(tagRepository.findByName("Tag 7")).thenReturn(afterEntity.getTags().iterator().next());
        when(giftCertificateRepository.findOne(4L)).thenReturn(beforeEntity);
        when(giftCertificateRepository.save(beforeEntity)).thenReturn(afterEntity);
        GiftCertificateDTO actualResult = giftCertificateService.updateGiftCertificate(4L, beforeEntity);
        GiftCertificateDTO expectedResult = createDTO3();
        Assertions.assertThat(actualResult).usingRecursiveComparison().ignoringFields("createDate", "lastUpdateDate").isEqualTo(expectedResult);

    }

    @Test
    public void updateGiftCertificateFailTest() {

        when(giftCertificateRepository.findOne(4L)).thenReturn(null);

        try {
            giftCertificateService.updateGiftCertificate(4L, giftCertificateNew2());
        } catch (SpringRestBasicException e) {
            Assertions.assertThat(e.getMessage()).isEqualTo("GiftCertificate not found with id: " + 4L);
            Assert.assertEquals(Optional.of("02"), Optional.ofNullable(e.getCode()));
            Assert.assertThrows(SpringRestBasicException.class, () -> {
                giftCertificateService.deleteGiftCertificate(4L);
            });
        }

    }

    @Test
    public void deleteCertificateFoundTest() {
        GiftCertificateEntity giftCertificate = new GiftCertificateEntity();
        giftCertificate.setId(1L);
        giftCertificate.setName("aa");
        giftCertificate.setDescription("bb");
        giftCertificate.setPrice(BigDecimal.valueOf(10));
        giftCertificate.setDuration(1);
        giftCertificate.setCreateDate(null);
        giftCertificate.setLastUpdateDate(null);
        giftCertificate.setTags(new HashSet<>());

        giftCertificateService.deleteGiftCertificate(1L);
        //       when(giftCertificateRepository.findOne(1L)).thenReturn(giftCertificate);
        Mockito.verify(giftCertificateRepository, Mockito.times(1)).findOne(1L);
        Mockito.verify(giftCertificateRepository, Mockito.times(1)).delete(giftCertificate);
    }

    @Test
    public void deleteCertificateNotFoundTest() {
        try {
            giftCertificateService.deleteGiftCertificate(4L);
        } catch (SpringRestBasicException e) {
            Assertions.assertThat(e.getMessage()).isEqualTo("GiftCertificate not found with id: " + 4L);
            Assert.assertEquals(Optional.of("02"), Optional.ofNullable(e.getCode()));
            Assert.assertThrows(SpringRestBasicException.class, () -> {
                giftCertificateService.deleteGiftCertificate(4L);
            });
        }
    }

    @Test
    public void patchMethodTest() {

        GiftCertificateEntity beforeEntity = giftCertificateNew();

        GiftCertificateEntity afterEntity = giftCertificateNew2();

        when(giftCertificateRepository.findOne(4L)).thenReturn(beforeEntity);
        when(giftCertificateRepository.save(beforeEntity)).thenReturn(afterEntity);
        GiftCertificateDTO actualResult = giftCertificateService.patchGiftCertificate(4L, beforeEntity);
        GiftCertificateDTO expectedResult = createDTO3();
        Assertions.assertThat(actualResult).usingRecursiveComparison().ignoringFields("createDate", "lastUpdateDate").isEqualTo(expectedResult);


    }

    @Test
    public void patchGiftCertificateAlreadyFoundTagNameTest() {

        GiftCertificateEntity beforeEntity = giftCertificateNew();

        GiftCertificateEntity afterEntity = giftCertificateNew2();
        when(tagRepository.findByName("Tag 7")).thenReturn(afterEntity.getTags().iterator().next());
        when(giftCertificateRepository.findOne(4L)).thenReturn(beforeEntity);
        when(giftCertificateRepository.save(beforeEntity)).thenReturn(afterEntity);
        GiftCertificateDTO actualResult = giftCertificateService.patchGiftCertificate(4L, beforeEntity);
        GiftCertificateDTO expectedResult = createDTO3();
        Assertions.assertThat(actualResult).usingRecursiveComparison().ignoringFields("createDate", "lastUpdateDate").isEqualTo(expectedResult);

    }

    @Test
    public void patchGiftCertificateFailTest() {

        GiftCertificateEntity beforeEntity = giftCertificateNew();


        when(giftCertificateRepository.findOne(4L)).thenReturn(null);
        try {
            giftCertificateService.patchGiftCertificate(4L, beforeEntity);
        } catch (SpringRestBasicException e) {
            Assertions.assertThat(e.getMessage()).isEqualTo("GiftCertificate not found with id: " + 4L);
            Assert.assertEquals(Optional.of("02"), Optional.ofNullable(e.getCode()));
            Assert.assertThrows(SpringRestBasicException.class, () -> {
                giftCertificateService.patchGiftCertificate(4L, beforeEntity);
            });
        }

    }


    private Iterable<GiftCertificateDTO> createDTOList() {

        GiftCertificateDTO tag = new GiftCertificateDTO();
        tag.setId(1L);
        tag.setName("aa");
        tag.setDescription("bb");
        tag.setPrice(BigDecimal.valueOf(10));
        tag.setDuration(1);
        tag.setCreateDate(null);
        tag.setLastUpdateDate(null);
        tag.setTags(new HashSet<>());

        GiftCertificateDTO tag2 = new GiftCertificateDTO();
        tag2.setId(2L);
        tag2.setName("bb");
        tag2.setDescription("bb");
        tag2.setPrice(BigDecimal.valueOf(20));
        tag2.setDuration(1);
        tag2.setCreateDate(null);
        tag2.setLastUpdateDate(null);
        tag2.setTags(new HashSet<>());

        GiftCertificateDTO tag3 = new GiftCertificateDTO();
        tag3.setId(3L);
        tag3.setName("ccaa");
        tag3.setDescription("bb");
        tag3.setPrice(BigDecimal.valueOf(30));
        tag3.setDuration(1);
        tag3.setCreateDate(null);
        tag3.setLastUpdateDate(null);
        tag3.setTags(new HashSet<>());

        List<GiftCertificateDTO> tagsDTO = List.of(tag, tag2, tag3);

        return tagsDTO;

    }

    private Iterable<GiftCertificateDTO> createDTOList2() {

        GiftCertificateDTO tag = new GiftCertificateDTO();
        tag.setId(1L);
        tag.setName("aa");
        tag.setDescription("bb");
        tag.setPrice(BigDecimal.valueOf(10));
        tag.setDuration(1);
        tag.setCreateDate(null);
        tag.setLastUpdateDate(null);
        tag.setTags(new HashSet<>());

        GiftCertificateDTO tag3 = new GiftCertificateDTO();
        tag3.setId(3L);
        tag3.setName("ccaa");
        tag3.setDescription("bb");
        tag3.setPrice(BigDecimal.valueOf(30));
        tag3.setDuration(1);
        tag3.setCreateDate(null);
        tag3.setLastUpdateDate(null);
        tag3.setTags(new HashSet<>());

        List<GiftCertificateDTO> tagsDTO = List.of(tag, tag3);

        return tagsDTO;

    }


    private GiftCertificateDTO createDTO() {

        GiftCertificateDTO tag = new GiftCertificateDTO();
        tag.setId(1L);
        tag.setName("aa");
        tag.setDescription("bb");
        tag.setPrice(BigDecimal.valueOf(10));
        tag.setDuration(1);
        tag.setCreateDate(null);
        tag.setLastUpdateDate(null);
        tag.setTags(new HashSet<>());
        return tag;

    }

    private GiftCertificateDTO createDTO2() {

        GiftCertificateDTO tag = new GiftCertificateDTO();
        tag.setId(4L);
        tag.setName("eeee");
        tag.setDescription("bb");
        tag.setPrice(BigDecimal.valueOf(10));
        tag.setDuration(1);
        tag.setCreateDate(null);
        tag.setLastUpdateDate(null);
        tag.setTags(new HashSet<>());
        return tag;

    }

    private GiftCertificateDTO createDTO3() {
        TagEntity tag1 = new TagEntity();
        tag1.setId(7L);
        tag1.setName("Tag 7");
        HashSet<TagEntity> tags1 = new HashSet<>();
        tags1.add(tag1);
        GiftCertificateDTO tag = new GiftCertificateDTO();
        tag.setId(4L);
        tag.setName("eeee");
        tag.setDescription("bb");
        tag.setPrice(BigDecimal.valueOf(10));
        tag.setDuration(1);
        tag.setCreateDate(null);
        tag.setLastUpdateDate(null);
        tag.setTags(tags1);
        return tag;

    }

    private Set<TagEntity> createDTO3Tags() {
        TagEntity tag1 = new TagEntity();
        tag1.setId(7L);
        tag1.setName("Tag 7");
        Set<TagEntity> tags1 = new HashSet<>();
        tags1.add(tag1);
        return tags1;

    }

    private GiftCertificateEntity giftCertificateNew() {
        TagEntity tag1 = new TagEntity();
        tag1.setId(7L);
        tag1.setName("Tag 7");
        HashSet<TagEntity> tags1 = new HashSet<>();
        tags1.add(tag1);

        GiftCertificateEntity tag = new GiftCertificateEntity();
        tag.setId(4L);
        tag.setName("eeee");
        tag.setDescription("bb");
        tag.setPrice(BigDecimal.valueOf(10));
        tag.setDuration(1);
        tag.setCreateDate(null);
        tag.setLastUpdateDate(null);
        tag.setTags(tags1);
        return tag;

    }

    private GiftCertificateEntity giftCertificateNew3() {


        GiftCertificateEntity tag = new GiftCertificateEntity();
        tag.setId(4L);
        tag.setName("eeee");
        tag.setDescription("bb");
        tag.setPrice(BigDecimal.valueOf(10));
        tag.setDuration(1);
        tag.setCreateDate(null);
        tag.setLastUpdateDate(null);
        tag.setTags(new HashSet<>());
        return tag;

    }

    private GiftCertificateEntity giftCertificateNew2() {
        TagEntity tag1 = new TagEntity();
        tag1.setId(7L);
        tag1.setName("Tag 7");
        HashSet<TagEntity> tags1 = new HashSet<>();
        tags1.add(tag1);


        GiftCertificateEntity tag = new GiftCertificateEntity();
        tag.setId(4L);
        tag.setName("eeee");
        tag.setDescription("bb");
        tag.setPrice(BigDecimal.valueOf(10));
        tag.setDuration(1);
        tag.setCreateDate(null);
        tag.setLastUpdateDate(null);
        tag.setTags(tags1);
        return tag;

    }

}