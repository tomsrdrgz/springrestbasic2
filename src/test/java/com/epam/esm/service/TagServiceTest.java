package com.epam.esm.service;

import com.epam.esm.repository.TagRepository;
import com.epam.esm.repository.entity.GiftCertificateEntity;
import com.epam.esm.repository.entity.TagEntity;
import com.epam.esm.service.mapper.TagMapper;
import com.epam.esm.web.dto.TagDTO;
import com.epam.esm.web.exception.SpringRestBasicException;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {


    @Mock
    private TagRepository tagRepository;
    private final TagMapper tagMapper = new TagMapper();

    @InjectMocks
    private TagService tagService;
    private List<TagEntity> tags = new ArrayList<>();;


    @Before
    public void setUp() {
        tagService = new TagService(tagRepository, tagMapper);

        TagEntity tag = new TagEntity();
        tag.setId(1L);
        tag.setName("aa");
        TagEntity tag2 = new TagEntity();
        tag2.setId(2L);
        tag2.setName("bb");
        TagEntity tag3 = new TagEntity();
        tag3.setId(3L);
        tag3.setName("ccaa");

        tags.add(tag);
        tags.add(tag2);
        tags.add(tag3);

        when(tagRepository.findAll()).thenReturn(tags);
        when(tagRepository.findOne(1L)).thenReturn(tag);

        when(tagRepository.findOne(4L)).thenReturn(null);






    }


    @Test
    public void getAllGiftCertificatesTest() {
        when(tagRepository.findAll()).thenReturn(tags);

        List<TagDTO> expectedResult = createDTOList();

        List<TagDTO> actualResult = tagService.getAllTags();

        Assertions.assertThat(actualResult).usingRecursiveComparison().isEqualTo(expectedResult);


    }

    @Test
    public void getTagByIdTest() {
        TagDTO tagDTO = tagService.getTag(1L);
        Assert.assertEquals("aa", tagDTO.getName());
        Assert.assertEquals(1L, tagDTO.getId().longValue());
    }


    @Test
    public void getTagByIdNotFoundTest() {
        try {
            tagService.getTag(4L);
        } catch (SpringRestBasicException e) {
            Assert.assertEquals("Could not find Tag with provided ID. " + 4L, e.getMessage());
            Assert.assertEquals(Optional.of("01"), Optional.ofNullable(e.getCode()));
        }
    }




    @Test
    public void createTagTest() {

        List<TagDTO> expectedResult = createDTOListForNewEntry();

        TagEntity tag = new TagEntity();
        List<TagEntity> actualResult = new ArrayList<>();
        tag.setId(4L);
        tag.setName("dd");
        actualResult.add(tag);
        when(tagRepository.save(tag)).thenReturn(tag);

        tagService.createTag(tag);
        Assertions.assertThat(actualResult).usingRecursiveComparison().isEqualTo(expectedResult);

    }

    @Test
    public void createTagAlreadyExistsWithThatName() {

        TagEntity tag = new TagEntity();
        List<TagEntity> actualResult = new ArrayList<>();
        tag.setId(3L);
        tag.setName("dd");
        actualResult.add(tag);
        when(tagRepository.findByName(tag.getName())).thenReturn(tag);

        try {
            tagService.createTag(tag);
        } catch (SpringRestBasicException e) {
            Assert.assertEquals("Tag with name " + tag.getName() + " already exist", e.getMessage());
            Assert.assertEquals(Optional.of("01"), Optional.ofNullable(e.getCode()));
        }

    }


    //create deletetag test fail and deletagTest ok
    @Test
    public void deleteTagTest() {

        TagEntity tag = new TagEntity();
        tag.setId(1L);
        tag.setName("aa");
        GiftCertificateEntity giftCertificateEntity = new GiftCertificateEntity();
        giftCertificateEntity.setId(1L);
        giftCertificateEntity.setName("Gift C 1");
        giftCertificateEntity.setDescription("Gift C 1");
        giftCertificateEntity.setPrice(null);
        giftCertificateEntity.setDuration(10);
        giftCertificateEntity.setCreateDate(null);
        giftCertificateEntity.setLastUpdateDate(null);
        Set<GiftCertificateEntity> giftCertificates = new HashSet<>();
        giftCertificates.add(giftCertificateEntity);
        tag.setGiftCertificates(giftCertificates);
        when(tagRepository.findOne(1L)).thenReturn(tag);


        tagService.deleteTag(1L);

        Mockito.verify(tagRepository, Mockito.times(1)).delete(tag);

    }

    @Test
public void deleteTagFailTest() {
        try {
            tagService.deleteTag(4L);
        } catch (SpringRestBasicException e) {
            Assert.assertEquals("Tag could not be found in Database with ID. " + 4L, e.getMessage());
            Assert.assertEquals(Optional.of("01"), Optional.ofNullable(e.getCode()));
            Assert.assertThrows(SpringRestBasicException.class, () -> tagService.deleteTag(4L));
        }
    }



    private List<TagDTO> createDTOList() {

        TagDTO tag = new TagDTO();
        tag.setId(1L);
        tag.setName("aa");
        tag.setGiftCertificates(new HashSet<>());
        TagDTO tag2 = new TagDTO();
        tag2.setId(2L);
        tag2.setName("bb");
        tag2.setGiftCertificates(new HashSet<>());
        TagDTO tag3 = new TagDTO();
        tag3.setId(3L);
        tag3.setName("ccaa");
        tag3.setGiftCertificates(new HashSet<>());
        return List.of(tag, tag2, tag3);

    }


    private List<TagDTO> createDTOListForSearchByName() {

        TagDTO tag = new TagDTO();
        tag.setId(1L);
        tag.setName("aa");
        tag.setGiftCertificates(new HashSet<>());
        return List.of(tag);
    }

    private List<TagDTO> createDTOListForSearchByNameContains() {

        TagDTO tag = new TagDTO();
        tag.setId(1L);
        tag.setName("aa");
        tag.setGiftCertificates(new HashSet<>());
        TagDTO tag2 = new TagDTO();
        tag2.setId(3L);
        tag2.setName("ccaa");
        tag2.setGiftCertificates(new HashSet<>());

        return List.of(tag, tag2);
    }

    private List<TagDTO> createDTOListForNewEntry() {

        TagDTO tag = new TagDTO();
        tag.setId(4L);
        tag.setName("dd");
        tag.setGiftCertificates(new HashSet<>());
        return List.of(tag);
    }

    private TagDTO createDTOListForUpdatedEntry() {

        TagDTO tag = new TagDTO();
        tag.setId(1L);
        tag.setName("dd");
        tag.setGiftCertificates(new HashSet<>());
        return tag;
    }


}

