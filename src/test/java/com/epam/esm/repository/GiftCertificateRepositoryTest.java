package com.epam.esm.repository;


import com.epam.esm.config.TestDataBaseConfig;
import com.epam.esm.repository.entity.GiftCertificateEntity;
import com.epam.esm.service.mapper.GiftCertificateMapper;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestDataBaseConfig.class)
@Transactional
public class GiftCertificateRepositoryTest {

    @Autowired
    GiftCertificateRepository giftCertificateRepository;

    private final GiftCertificateMapper giftCertificateMapper = new GiftCertificateMapper();

    @Before
    public void setUp() {
        // You can initialize test data here if needed
    }

    @Test
    public void checkSizeTest() {
        Iterable<GiftCertificateEntity> giftCertificates = giftCertificateRepository.findAll();
        int size = 0;
        for (GiftCertificateEntity giftCertificate : giftCertificates) {
            size++;
        }
        Assert.assertEquals(3, size);
    }

    @Test
    public void checkEntries() {
        GiftCertificateEntity giftCertificateEntity = giftCertificateRepository.findOne(1L);
        Assert.assertNotEquals("name1", giftCertificateEntity.getName());
        Assert.assertEquals("Gift C 1", giftCertificateEntity.getName());
        Assert.assertEquals(1L, giftCertificateEntity.getId().longValue());

        giftCertificateEntity = giftCertificateRepository.findOne(2L);
        Assert.assertNotEquals("name1", giftCertificateEntity.getName());
        Assert.assertEquals("Gift C 2", giftCertificateEntity.getName());
        Assert.assertEquals(2L, giftCertificateEntity.getId().longValue());
    }

    @Test
    public void findNoEntry(){
     try{
         giftCertificateRepository.findOne(4L);
     }catch (Exception e){
         Assert.assertEquals("Could not find GiftCertificate with provided ID. 4", e.getMessage());
     }

    }

    @Test
    public void saveEntry(){

        GiftCertificateEntity giftCertificateEntity = new GiftCertificateEntity();
        giftCertificateEntity.setId(4L);
        giftCertificateEntity.setName("name4");
        giftCertificateEntity.setDescription("description4");
        giftCertificateEntity.setPrice(new BigDecimal(4));
        giftCertificateEntity.setDuration(4);
        giftCertificateEntity.setCreateDate(null);
        giftCertificateEntity.setLastUpdateDate(null);
        GiftCertificateEntity savedGiftCertificateEntity = giftCertificateRepository.save(giftCertificateEntity);
        Assert.assertEquals("name4", savedGiftCertificateEntity.getName());
        Assert.assertEquals("description4", savedGiftCertificateEntity.getDescription());
        Assert.assertEquals(new BigDecimal(4), savedGiftCertificateEntity.getPrice());
        Assert.assertEquals(Optional.of(4).get(), savedGiftCertificateEntity.getDuration());
        Assert.assertNull(savedGiftCertificateEntity.getCreateDate());
        Assert.assertNull(savedGiftCertificateEntity.getLastUpdateDate());
        Assert.assertEquals(4L, savedGiftCertificateEntity.getId().longValue());

        Assertions.assertThat(savedGiftCertificateEntity).usingRecursiveComparison()
                .ignoringFields("createDate", "lastUpdateDate").isEqualTo(giftCertificateEntity);


    }

}
