package com.epam.esm.repository;



import com.epam.esm.config.TestDataBaseConfig;
import com.epam.esm.repository.entity.TagEntity;
import com.epam.esm.service.mapper.TagMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
    @ContextConfiguration(classes = TestDataBaseConfig.class)
    @Transactional
    public class TagRepositoryTest {


        @Autowired
        TagRepository tagRepository;

        private final TagMapper tagMapper = new TagMapper();

        @Before
        public void setUp() {

        }

        @Test
        public void checkSizeTest() {
            Iterable<TagEntity> tags = tagRepository.findAll();
            int size = 0;
            for (TagEntity tag : tags) {
                size++;
            }
            Assert.assertEquals(3, size);
        }

        @Test
        public void checkEntries() {
            TagEntity tagEntity = tagRepository.findOne(1L);
            Assert.assertNotEquals("name1", tagEntity.getName());
            Assert.assertEquals("Tag 1", tagEntity.getName());
            Assert.assertEquals(1L, tagEntity.getId().longValue());

            tagEntity = tagRepository.findOne(2L);
            Assert.assertNotEquals("name1", tagEntity.getName());
            Assert.assertEquals("Tag 2", tagEntity.getName());
            Assert.assertEquals(2L, tagEntity.getId().longValue());
        }

        @Test
        public void checkFindByName() {
            TagEntity tagEntity =  tagRepository.findByName("Tag 1");
            Assert.assertEquals(1L, tagEntity.getId().longValue());
            Assert.assertEquals("Tag 1", tagEntity.getName());

        }

        @Test
        public void checkFindByNameFail() {
            TagEntity tagEntity =  tagRepository.findByName("Tag");
            Assert.assertNull(tagEntity);

        }

        @Test
        public void checkFindByNameContains() {
            List<TagEntity> tagEntity =  tagRepository.findNameContains("Tag");
            Assert.assertEquals(1L, tagEntity.get(0).getId().longValue());
            Assert.assertEquals("Tag 1", tagEntity.get(0).getName());
            Assert.assertEquals(2L, tagEntity.get(1).getId().longValue());
            Assert.assertEquals("Tag 2", tagEntity.get(1).getName());
            Assert.assertEquals(3L, tagEntity.get(2).getId().longValue());
            Assert.assertEquals("Tag 3", tagEntity.get(2).getName());


        }

        @Test
        public void findByNameContainsFailTest(){

            List<TagEntity> tagEntity =  tagRepository.findNameContains("Noone 1");
            Assert.assertEquals(0, tagEntity.size());


    }

        @Test
        public void findNoEntry(){
         try{
             tagRepository.findOne(4L);
         }catch (Exception e){
             Assert.assertEquals("Could not find Tag with provided ID. 4", e.getMessage());
         }

        }

        @Test
        public void saveEntry(){
            TagEntity tagEntity = new TagEntity();
            tagEntity.setId(4L);
            tagEntity.setName("name4");
            TagEntity tagEntity2 = tagRepository.save(tagEntity);
            Assert.assertEquals("name4", tagEntity2.getName());
            Assert.assertEquals(4L, tagEntity2.getId().longValue());
        }

        @Test
        public void deleteEntry(){
            tagRepository.delete(1L);
            try{
                tagRepository.findOne(1L);
            }catch (Exception e){
                Assert.assertEquals("Could not find Tag with provided ID. 1", e.getMessage());
            }
        }

        @Test
        public void updateEntry(){

            TagEntity tagEntity = new TagEntity();
            tagEntity.setId(1L);
            tagEntity.setName("name4");
            tagRepository.save(tagEntity);
            TagEntity tagEntity2 = tagRepository.findOne(1L);
            Assert.assertEquals("name4", tagEntity2.getName());
            Assert.assertEquals(1L, tagEntity2.getId().longValue());
        }


}
