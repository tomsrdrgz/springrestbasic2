package com.epam.esm.repository;


import com.epam.esm.config.TestDataBaseConfig;
import com.epam.esm.repository.entity.GiftCertificateEntity;
import com.epam.esm.repository.entity.TagEntity;
import com.epam.esm.web.dto.SortBy;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestDataBaseConfig.class})
@Transactional
public class GiftCertificateQueryRepositoryImplTest {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private GiftCertificateQueryRepository giftCertificateQueryRepository;

    @Autowired
    private GiftCertificateQueryRepositoryImpl giftCertificateQueryRepositoryImpl;

    // Example test method
    @Test
    public void testFindByTagName() {

        String tagName = "Tag 2";
        SortBy sortBy = SortBy.NAME_ASC;


        List<GiftCertificateEntity> result = giftCertificateQueryRepository.findByTagName(tagName, sortBy);
        System.out.println(result);
        Set<TagEntity> tags = new HashSet<>();
        TagEntity tagEntity = new TagEntity();
        tagEntity.setName("Tag 2");
        tagEntity.setId(2L);
        tags.add(tagEntity);
        GiftCertificateEntity expected = createGiftCertificateEntity(1L, "Gift C 1", "Gift C 1", BigDecimal.valueOf(10.00), 10, null, null, tags);

        assertNotNull(result);
        Assertions.assertThat(result).hasSize(1);
        Assertions.assertThat(result.get(0)).usingRecursiveComparison().ignoringFields("createDate", "lastUpdateDate", "tags", "price").isEqualTo(expected);
    }

    @Test
    public void testFindByNameOrDescriptionContaining() {

        String searchString = "Gift C 1";
        SortBy sortBy = SortBy.NAME_ASC;

        List<GiftCertificateEntity> result = giftCertificateQueryRepository.findByNameOrDescriptionContaining(searchString, sortBy);
        System.out.println(result);
        Set<TagEntity> tags = new HashSet<>();
        TagEntity tagEntity = new TagEntity();
        tagEntity.setName("Tag 2");
        tagEntity.setId(2L);
        tags.add(tagEntity);
        GiftCertificateEntity expected = createGiftCertificateEntity(1L, "Gift C 1", "Gift C 1", BigDecimal.valueOf(10.00), 10, null, null, tags);

        assertNotNull(result);
        Assertions.assertThat(result).hasSize(1);
        Assertions.assertThat(result.get(0)).usingRecursiveComparison().ignoringFields("createDate", "lastUpdateDate", "tags", "price").isEqualTo(expected);
    }

    @Test
    public void testFindByNameOrDescriptionContainingNotFoundTest() {

        String searchString = "Gift C 7777";
        SortBy sortBy = SortBy.NAME_ASC;

        List<GiftCertificateEntity> result = giftCertificateQueryRepository.findByNameOrDescriptionContaining(searchString, sortBy);
        List<GiftCertificateEntity> expected = new ArrayList<>();

        assertEquals(result,expected);

    }

    @Test
    public void testFindByTagOrNameOrDescriptionContaining() {

        String tagName = "Tag 2";
        String searchString = "Gift C";
        SortBy sortBy = SortBy.NAME_ASC;

        List<GiftCertificateEntity> result = giftCertificateQueryRepository.findByTagAndNameOrDescriptionContaining(tagName, searchString, sortBy);
        System.out.println(result);
        Set<TagEntity> tags = new HashSet<>();
        TagEntity tagEntity = new TagEntity();
        tagEntity.setName("Tag 2");
        tagEntity.setId(2L);
        tags.add(tagEntity);
        GiftCertificateEntity expected = createGiftCertificateEntity(1L, "Gift C 1", "Gift C 1", BigDecimal.valueOf(10.00), 10, null, null, tags);

        assertNotNull(result);
        Assertions.assertThat(result).hasSize(1);
        Assertions.assertThat(result.get(0)).usingRecursiveComparison().ignoringFields("createDate", "lastUpdateDate", "tags", "price").isEqualTo(expected);
    }


    private GiftCertificateEntity createGiftCertificateEntity(Long id, String name, String description, BigDecimal price, int duration, Timestamp createDate, Timestamp lastUpdateDate, Set<TagEntity> tags) {
        GiftCertificateEntity giftCertificateEntity = new GiftCertificateEntity();
        giftCertificateEntity.setId(id);
        giftCertificateEntity.setName(name);
        giftCertificateEntity.setDescription(description);
        giftCertificateEntity.setPrice(price);
        giftCertificateEntity.setDuration(duration);
        giftCertificateEntity.setCreateDate(createDate);
        giftCertificateEntity.setLastUpdateDate(lastUpdateDate);
        giftCertificateEntity.setTags(tags);
        return giftCertificateEntity;
    }

}
