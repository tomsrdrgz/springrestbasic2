package com.epam.esm.controller;

import com.epam.esm.service.mapper.GiftCertificateMapper;
import com.epam.esm.web.dto.GiftCertificateDTO;
import com.epam.esm.repository.entity.GiftCertificateEntity;
import com.epam.esm.repository.entity.TagEntity;
import com.epam.esm.service.GiftCertificateService;
import com.epam.esm.web.controller.GiftCertificateController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class GiftCertificateControllerIntegrationTest {

    private MockMvc mockMvc;
    @Mock
    private GiftCertificateMapper giftCertificateMapper;
    @Mock
    private GiftCertificateService giftCertificateService;

    @InjectMocks
    private GiftCertificateController giftCertificateController;
    private GiftCertificateDTO sampleGiftCertificateDTO;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(giftCertificateController).build();
    }


    @Test
    public void testGetAllGC() throws Exception {

        mockMvc.perform(get("/giftCertificates")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    public void testGetGift() throws Exception {
        when(giftCertificateService.getOneGiftCertificates(anyLong())).thenReturn(sampleGiftCertificateDTO);

        mockMvc.perform(get("/giftCertificates/{giftCertificateId}", 1L)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }


    @Test
    public void testCreateGIftCertificate() throws Exception {

        mockMvc.perform(post("/giftCertificates")
                        .content("{\"name\":\"SampleTag\",\"description\":\"SampleTag\",\"duration\":10,\"price\":30,\"createDate\":null,\"lastUpdateDate\":null}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void testUpdateTag() throws Exception {

        mockMvc.perform(put("/giftCertificates/{giftCertificateId}", 1L)
                        .content("{\"name\":\"SampleTag\",\"description\":\"SampleTag\",\"duration\":10,\"price\":30,\"createDate\":null,\"lastUpdateDate\":null}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteTag() throws Exception {
        mockMvc.perform(delete("/giftCertificates/{giftCertificateId}", 1L)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    // Add more tests as needed for exception handling and other scenarios
}

