package com.epam.esm.controller;

import com.epam.esm.repository.entity.TagEntity;
import com.epam.esm.service.TagService;
import com.epam.esm.web.controller.TagController;
import com.epam.esm.web.dto.TagDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class TagControllerIntegrationTest {

    private MockMvc mockMvc;

    @Mock
    private TagService tagService;

    @InjectMocks
    private TagController tagController;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(tagController).build();
    }


    @Test
    public void testGetAllTags() throws Exception {


        mockMvc.perform(get("/tags")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetTag() throws Exception {


        mockMvc.perform(get("/tags" +
                        "/{tagId}", 1L)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateTag() throws Exception {

        mockMvc.perform(post("/tags")
                        .content("{\"name\":\"SampleTag2\"}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void testDeleteTag() throws Exception {
        mockMvc.perform(delete("/tags/{tagId}", 1L)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

}
